/* -------------------------------------------- */
export class RdDItemCompetenceCreature extends Item {

  /* -------------------------------------------- */
  static setRollDataCreature(rollData) {
    rollData.carac = { "carac_creature": { label: rollData.competence.name, value: rollData.competence.data.carac_value } };
    rollData.competence = duplicate(rollData.competence);
    rollData.competence.data.defaut_carac = "carac_creature";
    rollData.competence.data.categorie = "creature";
    rollData.selectedCarac = rollData.carac.carac_creature;
    if (rollData.competence.data.iscombat) {
      rollData.arme = RdDItemCompetenceCreature.toArme(rollData.competence);
    }

  }

  /* -------------------------------------------- */
  static toArme(item) {
    if (RdDItemCompetenceCreature.isCompetenceAttaque(item)) {
      let arme = { name: item.name, data: duplicate(item.data) };
      mergeObject(arme.data,
        {
          competence: item.name,
          resistance: 100,
          equipe: true,
          penetration: 0,
          force: 0,
          rapide: true
        });
      return arme;
    }
    console.error("RdDItemCompetenceCreature.toArme(", item, ") : impossible de transformer l'Item en arme");
    return undefined;
  }

  /* -------------------------------------------- */
  static isCompetenceAttaque(item) {
    return item.type == 'competencecreature' && item.data.iscombat;
  }

  /* -------------------------------------------- */
  static isCompetenceParade(item) {
    return item.type == 'competencecreature' && item.data.isparade;
  }
}  
