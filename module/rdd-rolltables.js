export class RdDRollTables {

  /* -------------------------------------------- */
  static async genericGetTableResult(tableName, toChat) {
    const pack = game.packs.get("foundryvtt-reve-de-dragon.tables-diverses");
    const index = await pack.getIndex();
    const entry = index.find(e => e.name === tableName);
    const table = await pack.getEntity(entry._id);
    const draw = await table.draw({ displayChat: toChat, rollMode: "gmroll"});
    console.log("RdDRollTables", tableName, toChat, ":", draw);
    return draw;
  }

  /* -------------------------------------------- */
  static async drawItemFromRollTable(tableName, toChat) {
    const draw = await RdDRollTables.genericGetTableResult(tableName, toChat);
    const drawnItemRef = draw.results.length > 0 ? draw.results[0] : undefined;
    if (drawnItemRef.collection) {
      const pack = game.packs.get(drawnItemRef.collection);
      return await pack.getEntity(drawnItemRef.resultId);
    }
    ui.notifications.warn("le tirage ne correspond pas à une entrée d'un Compendium")
    return drawnItemRef.text;
  }
  
  /* -------------------------------------------- */
  static async drawTextFromRollTable(tableName, toChat) {
    const draw = await RdDRollTables.genericGetTableResult(tableName, toChat);
    const drawnItemRef = draw.results.length > 0 ? draw.results[0] : undefined;
    if (drawnItemRef.collection) {
      ui.notifications.warn("le tirage correspond à une entrée d'un Compendium, on attendait un texte")
      return await pack.getEntity(drawnItemRef.resultId);
    }
    return drawnItemRef.text;
  }

  /* -------------------------------------------- */
  static async getCompetence(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Détermination aléatoire de compétence", toChat);
  }

  /* -------------------------------------------- */
  static async getSouffle(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Souffles de Dragon", toChat);
  }

  /* -------------------------------------------- */
  static async getQueue(toChat = false) {
    let queue = await RdDRollTables.drawItemFromRollTable("Queues de dragon", toChat);
    if (queue.name.toLowerCase().includes('lancinant') ) {
      queue = await RdDRollTables.drawItemFromRollTable("Désirs lancinants", toChat);
    }
    if (queue.name.toLowerCase().includes('fixe') ) {
      queue =  await RdDRollTables.drawItemFromRollTable("Idées fixes", toChat);
    }
    return queue;
  }

  /* -------------------------------------------- */
  static async getTeteHR(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Têtes de Dragon pour haut-rêvants", toChat);
  }

  /* -------------------------------------------- */
  static async getTete(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Têtes de Dragon pour tous personnages", toChat);
  }

  /* -------------------------------------------- */
  static async getOmbre(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Ombre de Thanatos", toChat);
  }

  /* -------------------------------------------- */
  static async getTarot(toChat = true) {
    return await RdDRollTables.drawItemFromRollTable("Tarot Draconique", toChat);
  }

  /* -------------------------------------------- */
  static async getMaladresse(options = {toChat: false, arme: false}) {
    return await RdDRollTables.drawTextFromRollTable(
      options.arme ? "Maladresse armé" : "Maladresses non armé",
      options.toChat);
  }
}
