import { RdDDice } from "./rdd-dice.js";

export class DeDraconique extends Roll{

  static async ddr(rollMode=undefined) {
    let ddr = new DeDraconique().evaluate();
    await RdDDice.show(ddr, rollMode);
    return ddr;
  }

  constructor(){
    super("1d8x8 - 0")
  }

  evaluate() {
    super.evaluate();
    const rerolls = Math.ceil(this.total / 8);
    this.terms[this.terms.length - 1] = rerolls;
    this.results[this.results.length - 1] = rerolls;
    this._total -= rerolls;
    return this;
  }

  async render(chatOptions) {
    return super.render(chatOptions)
  }
}