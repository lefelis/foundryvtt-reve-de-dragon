
/**
 * Class providing helper methods to get the list of users, and 
 */
export class ChatUtility {

  /* -------------------------------------------- */
  static onSocketMessage(sockmsg) {
    switch (sockmsg.msg) {
      case "msg_delete_chat_message": return ChatUtility.onRemoveMessages(sockmsg.part, sockmsg.gmId);
    }
  }

  /* -------------------------------------------- */
  static onRemoveMessages(part, gmId) {
    if (game.user._id == gmId) {
      const toDelete = game.messages.filter(it => it.data.content.includes(part));
      toDelete.forEach(it => it.delete());
    }

   }
  
  /* -------------------------------------------- */
  static removeChatMessageContaining(part) {
    const gmId = game.user.isGM ? game.user._id : game.users.entities.find(u => u.isGM)?.id;

    if (!gmId || game.user.isGM) {
      ChatUtility.onRemoveMessages(part, game.user._id);
    }
    else {
      game.socket.emit("system.foundryvtt-reve-de-dragon", {
        msg: "msg_delete_chat_message", data: {
          part:part,
          gmId: gmId,
        }});
    }
  }

  /* -------------------------------------------- */
  static createChatWithRollMode(name, chatOptions) {
    ChatUtility.createChatMessage(name, game.settings.get("core", "rollMode"), chatOptions);
  }

  /* -------------------------------------------- */
  static createChatMessage(name, rollMode, chatOptions) {
    switch (rollMode) {
      case "blindroll": // GM only
        if (!game.user.isGM) {
          ChatUtility.blindMessageToGM(chatOptions);

          chatOptions.whisper = [game.user._id];
          chatOptions.content = "Message envoyé en aveugle au Gardien";
        }
        else {
          chatOptions.whisper = ChatUtility.getUsers(user => user.isGM);
        }
        break;
      default:
        chatOptions.whisper = ChatUtility.getWhisperRecipients(rollMode, name);
        break;
    }
    chatOptions.alias = chatOptions.alias || name;
    ChatMessage.create(chatOptions);
  }

  /* -------------------------------------------- */
  static prepareChatMessage(rollMode, name) {
    return {
      user: game.user._id,
      whisper: ChatUtility.getWhisperRecipients(rollMode, name)
    }
  }

  /* -------------------------------------------- */
  static getWhisperRecipients(rollMode, name) {
    switch (rollMode) {
      case "blindroll": return ChatUtility.getUsers(user => user.isGM);
      case "gmroll": return ChatUtility.getWhisperRecipientsAndGMs(name);
      case "selfroll": return [game.user._id];
    }
    return undefined;
  }

  /* -------------------------------------------- */
  static getWhisperRecipientsAndGMs(name) {
    return ChatMessage.getWhisperRecipients(name)
      .concat(ChatMessage.getWhisperRecipients('GM'));
  }

  /* -------------------------------------------- */
  static getUsers(filter) {
    return game.users.filter(filter).map(user => user.data._id);
  }

  /* -------------------------------------------- */
  static blindMessageToGM(chatOptions) {
    let chatGM = duplicate(chatOptions);
    chatGM.whisper = ChatUtility.getUsers(user => user.isGM);
    chatGM.content = "Message aveugle de " + game.user.name + "<br>" + chatOptions.content;
    console.log("blindMessageToGM", chatGM);
    game.socket.emit("system.foundryvtt-reve-de-dragon", { msg: "msg_gm_chat_message", data: chatGM });
  }

  /* -------------------------------------------- */
  static handleGMChatMessage(data) {
    console.log("blindMessageToGM", data);
    if (game.user.isGM) { // message privé pour GM only
      data.user = game.user._id;
      ChatMessage.create(data);
    }
  }
  
}
