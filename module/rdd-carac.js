import { Grammar } from "./grammar.js";

export class RdDCarac {

  static isAgiliteOuDerivee(selectedCarac) {
    return selectedCarac?.label.match(/(Agilité|Dérobée)/);
  }
  static isVolonte(selectedCarac) {
    return selectedCarac?.label == 'Volonté';
  }
  static isChance(selectedCarac) {
    return selectedCarac?.label?.toLowerCase()?.match(/chance( actuelle)?/);
  }
  static isReve(selectedCarac) {
    return selectedCarac?.label?.toLowerCase()?.match(/r(e|ê)ve(( |-)actuel)?/);
  }

  static isIgnoreEtatGeneral(selectedCarac, competence) {
    return !selectedCarac ||
      RdDCarac.isChance(selectedCarac) ||
      (RdDCarac.isReve(selectedCarac) && !competence);
  }

  /**
   * L’appel à la chance n’est possible que pour recommencer les jets d’actions physiques :
   * tous les jets de combat, de FORCE, d’AGILITÉ, de DEXTÉRITÉ, de Dérobée, d’APPARENCE,
   * ainsi que de Perception active et volontaire.
   */
  static isActionPhysique(selectedCarac) {
    return Grammar.toLowerCaseNoAccent(selectedCarac?.label).match(/(apparence|force|agilite|dexterite|vue|ouie|odorat|empathie|melee|tir|lancer|derobee)/);
  }
}
