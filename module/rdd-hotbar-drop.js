
export class RdDHotbar {

  /**
   * Create a macro when dropping an entity on the hotbar
   * Item      - open roll dialog for item
   * Actor     - open actor sheet
   * Journal   - open journal sheet
   */
  static initDropbar( ) {

    Hooks.on("hotbarDrop", async (bar, data, slot) => {
    // Create item macro if rollable item - weapon, spell, prayer, trait, or skill
    if (data.type == "Item") {
      if (data.data.type != "arme" && data.data.type != "competence" )
        return
      let item = data.data
      let command = `game.system.rdd.RdDHotbar.rollMacro("${item.name}", "${item.type}");`;
      let macro = game.macros.entities.find(m => (m.name === item.name) && (m.command === command));
      if (!macro) {
        macro = await Macro.create({
          name: item.name,
          type: "script",
          img: item.img,
          command: command
        }, { displaySheet: false })
      }
      game.user.assignHotbarMacro(macro, slot);
    }
    // Create a macro to open the actor sheet of the actor dropped on the hotbar
    else if (data.type == "Actor") {
      let actor = game.actors.get(data.id);
      let command = `game.actors.get("${data.id}").sheet.render(true)`
      let macro = game.macros.entities.find(m => (m.name === actor.name) && (m.command === command));
      if (!macro) {
        macro = await Macro.create({
          name: actor.data.name,
          type: "script",
          img: actor.data.img,
          command: command
        }, { displaySheet: false })
        game.user.assignHotbarMacro(macro, slot);
      }
    }
    // Create a macro to open the journal sheet of the journal dropped on the hotbar
    else if (data.type == "JournalEntry") {
      let journal = game.journal.get(data.id);
      let command = `game.journal.get("${data.id}").sheet.render(true)`
      let macro = game.macros.entities.find(m => (m.name === journal.name) && (m.command === command));
      if (!macro) {
        macro = await Macro.create({
          name: journal.data.name,
          type: "script",
          img: "systems/wfrp4e/icons/buildings/scroll.png",
          command: command
        }, { displaySheet: false })
        game.user.assignHotbarMacro(macro, slot);
      }
    }
    return false;
  });
  }

  /** Roll macro */
  static rollMacro(itemName, itemType, bypassData) {
    const speaker = ChatMessage.getSpeaker();
    let actor;
    if (speaker.token) actor = game.actors.tokens[speaker.token];
    if (!actor) actor = game.actors.get(speaker.actor);
    let item = actor ? actor.items.find(i => i.name === itemName && i.type == itemType) : null;
    if (!item) return ui.notifications.warn(`Impossible de trouver l'objet de cette macro`);

    item = item.data;

    // Trigger the item roll
    switch (item.type) {
      case "arme":
        return actor.rollArme(item.data.competence, itemName);
      case "competence":
        return actor.rollCompetence( itemName );
    }
  }

}
