import { RdDUtility } from "./rdd-utility.js";

/* -------------------------------------------- */
export class RdDItem extends Item {

  /* -------------------------------------------- */
  async postItem() {
    console.log(this);
    const properties = this[`_${this.data.type}ChatData`]();
    let chatData = duplicate(this.data);
    chatData["properties"] = properties

    //Check if the posted item should have availability/pay buttons
    chatData.hasPrice = "cout" in chatData.data;
    chatData.data.cout_deniers = 0;

    let dialogResult = [-1, -1]; // dialogResult[0] = quantité, dialogResult[1] = prix
    if (chatData.hasPrice )
    {
      let sols = chatData.data.cout;
      chatData.data.cout_deniers = Math.floor(sols * 100);
      dialogResult = await new Promise( (resolve, reject) => {new Dialog({
          content : 
          `<p>Modifier la quantité?</p>
          <div class="form-group">
            <label> Quantité</label>
            <input name="quantity" type="text" value="1"/>
          </div>
          <p>Modifier la prix?</p>
          <div class="form-group">
            <label> Prix en Sols</label>
            <input name="price" type="text" value="${chatData.data.cout}"/>
          </div>
          `,
          title : "Quantité & Prix",
          buttons : {
            post : {
              label : "Soumettre",
              callback: (dlg) => {
                resolve( [ dlg.find('[name="quantity"]').val(), dlg.find('[name="price"]').val() ] )
              }
            },
          }
        }).render(true)
      })
    } 
    
    if (dialogResult[0] > 0)
    {
      if (this.isOwned)
      {
        if (this.data.data.quantite == 0)
          dialogResult[0] = -1
        else if (this.data.data.quantite < dialogResult[0])
        {
          dialogResult[0] = this.data.data.quantite;
          ui.notifications.notify(`Impossible de poster plus que ce que vous avez. La quantité à été réduite à ${dialogResult[0]}.`) 
          this.update({"data.quantite" : 0})
        }
        else {
          ui.notifications.notify(`Quantité réduite par ${dialogResult[0]}.`) 
          this.update({"data.quantite" : this.data.data.quantite - dialogResult[0]})
        }
      }
    }
    
    if ( chatData.hasPrice ) {
      if (dialogResult[0] > 0)
        chatData.postQuantity = Number(dialogResult[0]);
      if (dialogResult[1] > 0) {
        chatData.postPrice = dialogResult[1];            
        chatData.data.cout_deniers = Math.floor(dialogResult[1] * 100); // Mise à jour cout en deniers
      }
      chatData.finalPrice = Number(chatData.postPrice) * Number(chatData.postQuantity);
      chatData.data.cout_deniers_total = chatData.data.cout_deniers * Number(chatData.postQuantity);
      chatData.data.quantite = chatData.postQuantity;
      console.log("POST : ", chatData.finalPrice, chatData.data.cout_deniers_total, chatData.postQuantity);
    }
    // Don't post any image for the item (which would leave a large gap) if the default image is used
    if (chatData.img.includes("/blank.png"))
      chatData.img = null;
    
    // JSON object for easy creation
    chatData.jsondata = JSON.stringify(
        {
          compendium : "postedItem",
          payload: this.data,
        });
        
    renderTemplate('systems/foundryvtt-reve-de-dragon/templates/post-item.html', chatData).then(html => {
      let chatOptions = RdDUtility.chatDataSetup(html);
      ChatMessage.create(chatOptions)
    });
  }

  /* -------------------------------------------- */
  _objetChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Encombrement</b>: ${data.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _armeChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Compétence</b>: ${data.competence}`,
      `<b>Dommages</b>: ${data.dommages}`,
      `<b>Force minimum</b>: ${data.force}`,
      `<b>Resistance</b>: ${data.resistance}`,
      `<b>Encombrement</b>: ${data.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _conteneurChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Capacité</b>: ${data.capacite} Enc.`,
      `<b>Encombrement</b>: ${data.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _munitionChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Encombrement</b>: ${data.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _armureChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Protection</b>: ${data.protection}`,
      `<b>Détérioration</b>: ${data.deterioration}`,
      `<b>Malus armure</b>: ${data.malus}`,
      `<b>Encombrement</b>: ${data.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _competenceChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Catégorie</b>: ${data.categorie}`,
      `<b>Niveau</b>: ${data.niveau}`,
      `<b>Caractéristique par défaut</b>: ${data.carac_defaut}`,
      `<b>XP</b>: ${data.xp}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _competencecreatureChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Catégorie</b>: ${data.categorie}`,
      `<b>Niveau</b>: ${data.niveau}`,
      `<b>Caractéristique</b>: ${data.carac_value}`,
      `<b>XP</b>: ${data.xp}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _sortChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Draconic</b>: ${data.draconic}`,
      `<b>Difficulté</b>: ${data.difficulte}`,
      `<b>Case TMR</b>: ${data.caseTMR}`,
      `<b>Points de Rêve</b>: ${data.ptreve}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _herbeChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Milieu</b>: ${data.milieu}`,
      `<b>Rareté</b>: ${data.rarete}`,
      `<b>Catégorie</b>: ${data.categorie}`,
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _ingredientChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Milieu</b>: ${data.milieu}`,
      `<b>Rareté</b>: ${data.rarete}`,
      `<b>Catégorie</b>: ${data.categorie}`,
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _tacheChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Caractéristique</b>: ${data.carac}`,
      `<b>Compétence</b>: ${data.competence}`,
      `<b>Périodicité</b>: ${data.periodicite}`,
      `<b>Fatigue</b>: ${data.fatigue}`,
      `<b>Difficulté</b>: ${data.difficulte}`,
      `<b>Points de Tâche</b>: ${data.points_de_tache}`,
      `<b>Points de Tâche atteints</b>: ${data.points_de_tache_courant}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _livreChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Compétence</b>: ${data.competence}`,
      `<b>Auteur</b>: ${data.auteur}`,
      `<b>Difficulté</b>: ${data.difficulte}`,
      `<b>Points de Tâche</b>: ${data.points_de_tache}`,
      `<b>Encombrement</b>: ${data.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _potionChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Rareté</b>: ${data.rarete}`,
      `<b>Catégorie</b>: ${data.categorie}`,
      `<b>Encombrement</b>: ${data.encombrement}`,
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _queueChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Refoulement</b>: ${data.refoulement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _ombreChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Refoulement</b>: ${data.refoulement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _souffleChatData() {
    const data = duplicate(this.data.data);
    let properties = [];
    return properties;
  }
  /* -------------------------------------------- */
  _teteChatData() {
    const data = duplicate(this.data.data);
    let properties = [];
    return properties;
  }
  /* -------------------------------------------- */
  _tarotChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Concept</b>: ${data.concept}`,
      `<b>Aspect</b>: ${data.aspect}`,
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _nombreastralChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Valeur</b>: ${data.value}`,
      `<b>Jour</b>: ${data.jourlabel}`,
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _monnaieChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Valeur en Deniers</b>: ${data.valeur_deniers}`,
      `<b>Encombrement</b>: ${data.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _meditationChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Thème</b>: ${data.theme}`,
      `<b>Compétence</b>: ${data.competence}`,
      `<b>Support</b>: ${data.support}`,
      `<b>Heure</b>: ${data.heure}`,
      `<b>Purification</b>: ${data.purification}`,
      `<b>Vêture</b>: ${data.veture}`,
      `<b>Comportement</b>: ${data.comportement}`,
      `<b>Case TMR</b>: ${data.tmr}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _casetmrChatData() {
    const data = duplicate(this.data.data);
    let properties = [
      `<b>Coordonnée</b>: ${data.coord}`,
      `<b>Spécificité</b>: ${data.specific}`
    ]
    return properties;
  }

}
