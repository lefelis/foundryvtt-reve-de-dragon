
const listeReglesOptionelles = [
  {name:'recul', group:'combat', descr:"Appliquer le recul en cas de particulière en force ou de charge"},
  {name:'resistanceArmeParade', group:'combat', descr:"Faire le jet de résistance des armes lors de parades pouvant les endommager"},
  {name:'deteriorationArmure', group:'combat', descr:"Tenir compte de la détérioration des armures"},
  {name:'defenseurDesarme', group:'combat', descr:"Le défenseur peut être désarmé en parant une particulière en force ou une charge avec une arme autre qu'un bouclier"},
  {name:'categorieParade', group:'combat', descr:"Le défenseur doit obtenir une significative en cas de parade avec des armes de catégories différentes"},
  {name:'tripleSignificative', group:'combat', descr:"En cas de demi-surprise, d'attaque particulière en finesse, et de catégories d'armes différentes, le défenseur doit obtenir 1/8 des chances de succès"},
  {name:'astrologie', group:'generale', descr:"Appliquer les ajustements astrologiques aux jets de chance et aux rituels"}
];

export class ReglesOptionelles extends FormApplication {
  static init() {
    for (const regle of listeReglesOptionelles) {
      const name = regle.name;
      const id = ReglesOptionelles._getIdRegle(name);
      game.settings.register("foundryvtt-reve-de-dragon", id, { name: id, scope: "world", config: false, default: regle.default??true, type: Boolean });
    }

    game.settings.registerMenu("foundryvtt-reve-de-dragon", "rdd-options-regles", {
      name: "Choisir les règles optionelles",
      label: "Choix des règles optionelles",
      hint: "Ouvre la fenêtre de sélection des règles optionelles",
      icon: "fas fa-bars",
      type: ReglesOptionelles,
      restricted: true
    });
  }

  constructor(...args) {
    super(...args);
  }

  static _getIdRegle(name) {
    return `rdd-option-${name}`;
  }

  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      id: "combat-settings",
      template: "systems/foundryvtt-reve-de-dragon/templates/regles-optionelles.html",
      height: 600,
      width: 350,
      minimizable: false,
      closeOnSubmit: true,
      title: "Options de combat"
    });
    return options;
  }

  getData() {
    let data = super.getData();
    data.regles = listeReglesOptionelles.map(it => {
      let r = duplicate(it);
      r.id = ReglesOptionelles._getIdRegle(r.name);
      r.active = ReglesOptionelles.isUsing(r.name); 
      return r;
    })
    return data;
  }

  static isUsing(name) {
    return game.settings.get("foundryvtt-reve-de-dragon", ReglesOptionelles._getIdRegle(name));
  }

  activateListeners(html) {
    html.find(".select-option").click((event) => {
      if (event.currentTarget.attributes.name) {
        let id = event.currentTarget.attributes.name.value;
        let isChecked = event.currentTarget.checked;
        game.settings.set("foundryvtt-reve-de-dragon", id, isChecked);
      }
    });
  }

  async _updateObject(event, formData) {
    this.close();
  }
}

