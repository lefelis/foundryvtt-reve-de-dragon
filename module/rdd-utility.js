/* Common useful functions shared between objects */

import { RdDRollTables } from "./rdd-rolltables.js";
import { ChatUtility } from "./chat-utility.js";
import { RdDCombat, RdDCombatManager } from "./rdd-combat.js";
import { RdDRollResolutionTable } from "./rdd-roll-resolution-table.js";
import { RdDItemCompetenceCreature } from "./item-competencecreature.js";
import { RdDItemArme } from "./item-arme.js";
import { RdDItemCompetence } from "./item-competence.js";
import { Misc } from "./misc.js";
import { Grammar } from "./grammar.js";

/* -------------------------------------------- */
const categorieCompetences = {
  "generale": { level: "-4", label: "Générales" },
  "particuliere": { level: "-8", label: "Particulières" },
  "specialisee": { level: "-11", label: "Spécialisées" },
  "connaissance": { level: "-11", label: "Connaissances" },
  "draconic": { level: "-11", label: "Draconics" },
  "melee": { level: "-6", label: "Mêlée" },
  "tir": { level: "-8", label: "Tir" },
  "lancer": { level: "-8", label: "Lancer" }
}

/* -------------------------------------------- */
const limitesArchetypes = [
  { "niveau": 0, "nombreMax": 100, "nombre": 0 },
  { "niveau": 1, "nombreMax": 10, "nombre": 0 },
  { "niveau": 2, "nombreMax": 9, "nombre": 0 },
  { "niveau": 3, "nombreMax": 8, "nombre": 0 },
  { "niveau": 4, "nombreMax": 7, "nombre": 0 },
  { "niveau": 5, "nombreMax": 6, "nombre": 0 },
  { "niveau": 6, "nombreMax": 5, "nombre": 0 },
  { "niveau": 7, "nombreMax": 4, "nombre": 0 },
  { "niveau": 8, "nombreMax": 3, "nombre": 0 },
  { "niveau": 9, "nombreMax": 2, "nombre": 0 },
  { "niveau": 10, "nombreMax": 1, "nombre": 0 },
  { "niveau": 11, "nombreMax": 1, "nombre": 0 }
];

/* -------------------------------------------- */
// This table starts at 0 -> niveau -10
const carac_array = ["taille", "apparence", "constitution", "force", "agilite", "dexterite", "vue", "ouie", "odoratgout", "volonte", "intellect", "empathie", "reve", "chance", "melee", "tir", "lancer", "derobee"];
const difficultesLibres = [0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10];
const ajustementsConditions = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, +1, +2, +3, +4, +5, +6, +7, +8, +9, +10];
const ajustementsEncaissement = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, +1, +2, +3, +4, +5, +6, +7, +8, +9, +10, +11, +12, +13, +14, +15, +16, +17, +18, +19, +20, +21, +22, +23, +24, +25];

const tableCaracDerivee = {
  // xp: coût pour passer du niveau inférieur à ce niveau
  1: { xp: 3, poids: "moins de 1kg", plusdom: -5, sconst: 0.5, sust: 0.1 },
  2: { xp: 3, poids: "1-5", plusdom: -4, sconst: 0.5, sust: 0.3 },
  3: { xp: 4, poids: "6-10", plusdom: -3, sconst: 1, sust: 0.5, beaute: 'hideux' },
  4: { xp: 4, poids: "11-20", plusdom: -3, sconst: 1, sust: 1, beaute: 'repoussant' },
  5: { xp: 5, poids: "21-30", plusdom: -2, sconst: 1, sust: 1, beaute: 'franchement très laid' },
  6: { xp: 5, poids: "31-40", plusdom: -1, sconst: 2, sust: 2, beaute: 'laid' },
  7: { xp: 6, poids: "41-50", plusdom: -1, sconst: 2, sust: 2, beaute: 'très désavantagé' },
  8: { xp: 6, poids: "51-60", plusdom: 0, sconst: 2, sust: 2, beaute: 'désavantagé' },
  9: { xp: 7, poids: "61-65", plusdom: 0, sconst: 3, sust: 2, beaute: 'pas terrible' },
  10: { xp: 7, poids: "66-70", plusdom: 0, sconst: 3, sust: 3, beaute: 'commun' },
  11: { xp: 8, poids: "71-75", plusdom: 0, sconst: 3, sust: 3, beaute: 'pas mal' },
  12: { xp: 8, poids: "76-80", plusdom: +1, sconst: 4, sust: 3, beaute: 'avantagé' },
  13: { xp: 9, poids: "81-90", plusdom: +1, sconst: 4, sust: 3, beaute: 'mignon' },
  14: { xp: 9, poids: "91-100", plusdom: +2, sconst: 4, sust: 4, beaute: 'beau' },
  15: { xp: 10, poids: "101-110", plusdom: +2, sconst: 5, sust: 4, beaute: 'très beau' },
  16: { xp: 20, poids: "111-120", plusdom: +3, sconst: 5, sust: 4, beaute: 'éblouissant' },
  17: { xp: 30, poids: "121-131", plusdom: +3, sconst: 5, sust: 5 },
  18: { xp: 40, poids: "131-141", plusdom: +4, sconst: 6, sust: 5 },
  19: { xp: 50, poids: "141-150", plusdom: +4, sconst: 6, sust: 5 },
  20: { xp: 60, poids: "151-160", plusdom: +4, sconst: 6, sust: 6 },
  21: { xp: 70, poids: "161-180", plusdom: +5, sconst: 7, sust: 6 },
  22: { xp: 80, poids: "181-200", plusdom: +5, sconst: 7, sust: 7 },
  23: { xp: 90, poids: "201-300", plusdom: +6, sconst: 7, sust: 8 },
  24: { xp: 100, poids: "301-400", plusdom: +6, sconst: 8, sust: 9 },
  25: { xp: 110, poids: "401-500", plusdom: +7, sconst: 8, sust: 10 },
  26: { xp: 120, poids: "501-600", plusdom: +7, sconst: 8, sust: 11 },
  27: { xp: 130, poids: "601-700", plusdom: +8, sconst: 9, sust: 12 },
  28: { xp: 140, poids: "701-800", plusdom: +8, sconst: 9, sust: 13 },
  29: { xp: 150, poids: "801-900", plusdom: +9, sconst: 9, sust: 14 },
  30: { xp: 160, poids: "901-1000", plusdom: +9, sconst: 10, sust: 15 },
  31: { xp: 170, poids: "1001-1500", plusdom: +10, sconst: 10, sust: 16 },
  32: { xp: 180, poids: "1501-2000", plusdom: +11, sconst: 10, sust: 17 }
}

/* -------------------------------------------- */
function _buildAllSegmentsFatigue(max) {
  const cycle = [5, 2, 4, 1, 3, 0];
  let fatigue = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]];
  for (let i = 0; i <= max; i++) {
    const ligneFatigue = duplicate(fatigue[i]);
    const caseIncrementee = cycle[i % 6];
    ligneFatigue[caseIncrementee]++;
    ligneFatigue[caseIncrementee + 6]++;
    ligneFatigue.fatigueMax = 2 * (i + 1);
    fatigue[i + 1] = ligneFatigue;
  }
  return fatigue;
}

/* -------------------------------------------- */
function _cumulSegmentsFatigue(matrix) {
  let cumulMatrix = [];
  for (let line of matrix) {
    let cumul = duplicate(line);

    for (let i = 1; i < 12; i++) {
      cumul[i] += cumul[i - 1];
    }
    cumulMatrix.push(cumul);
  }
  return cumulMatrix;
}

/* -------------------------------------------- */
const fatigueMatrix = _buildAllSegmentsFatigue(60);
const cumulFatigueMatrix = _cumulSegmentsFatigue(fatigueMatrix);

const fatigueMalus = [0, 0, 0, -1, -1, -1, -2, -3, -4, -5, -6, -7]; // Provides the malus for each segment of fatigue
const fatigueLineSize = [3, 6, 7, 8, 9, 10, 11, 12];
const fatigueLineMalus = [0, -1, -2, -3, -4, -5, -6, -7];
const fatigueMarche = {
  "aise": { "4": 1, "6": 2, "8": 3, "10": 4, "12": 6 },
  "malaise": { "4": 2, "6": 3, "8": 4, "10": 6 },
  "difficile": { "4": 3, "6": 4, "8": 6 },
  "tresdifficile": { "4": 4, "6": 6 }
}

/* -------------------------------------------- */
const definitionsBlessures = [
  { type: "legere", facteur: 2 },
  { type: "grave", facteur: 4 },
  { type: "critique", facteur: 6 }
]

/* -------------------------------------------- */
const nomEthylisme = ["Emeché", "Gris", "Pinté", "Pas frais", "Ivre", "Bu", "Complètement fait", "Ivre mort"];

/* -------------------------------------------- */
const definitionsEncaissement = {
  "mortel": [
    { minimum: undefined, maximum: 0, endurance: "0", vie: "0", eraflures: 0, legeres: 0, graves: 0, critiques: 0 },
    { minimum: 1, maximum: 10, endurance: "1d4", vie: "0", eraflures: 1, legeres: 0, graves: 0, critiques: 0 },
    { minimum: 11, maximum: 15, endurance: "1d6", vie: "0", eraflures: 0, legeres: 1, graves: 0, critiques: 0 },
    { minimum: 16, maximum: 19, endurance: "2d6", vie: "2", eraflures: 0, legeres: 0, graves: 1, critiques: 0 },
    { minimum: 20, maximum: undefined, endurance: "100", vie: "4 + @over20", eraflures: 0, legeres: 0, graves: 0, critiques: 1 },
  ],
  "non-mortel": [
    { minimum: undefined, maximum: 0, endurance: "0", vie: "0", eraflures: 0, legeres: 0, graves: 0, critiques: 0 },
    { minimum: 1, maximum: 10, endurance: "1d4", vie: "0", eraflures: 1, legeres: 0, graves: 0, critiques: 0 },
    { minimum: 11, maximum: 15, endurance: "1d6", vie: "0", eraflures: 1, legeres: 0, graves: 0, critiques: 0 },
    { minimum: 16, maximum: 19, endurance: "2d6", vie: "0", eraflures: 0, legeres: 1, graves: 0, critiques: 0 },
    { minimum: 20, maximum: undefined, endurance: "100", vie: "0", eraflures: 0, legeres: 1, graves: 0, critiques: 0 },
  ],
  "cauchemar": [
    { minimum: undefined, maximum: 0, endurance: "0", vie: "0", eraflures: 0, legeres: 0, graves: 0, critiques: 0 },
    { minimum: 1, maximum: 10, endurance: "1d4", vie: "0", eraflures: 1, legeres: 0, graves: 0, critiques: 0 },
    { minimum: 11, maximum: 15, endurance: "1d6", vie: "0", eraflures: 1, legeres: 0, graves: 0, critiques: 0 },
    { minimum: 16, maximum: 19, endurance: "2d6", vie: "0", eraflures: 1, legeres: 0, graves: 0, critiques: 0 },
    { minimum: 20, maximum: undefined, endurance: "3d6 + @over20", vie: "0", eraflures: 1, legeres: 0, graves: 0, critiques: 0 },
  ]
};

/* -------------------------------------------- */
export class RdDUtility {
  /* -------------------------------------------- */
  static async preloadHandlebarsTemplates() {
    const templatePaths = [
      //Character Sheets
      'systems/foundryvtt-reve-de-dragon/templates/actor-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/actor-creature-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/actor-entite-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/actor-vehicule-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/actor-sheet-competence-partial.html',
      //Items
      'systems/foundryvtt-reve-de-dragon/templates/item-competence-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-competencecreature-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-arme-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-armure-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-objet-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-conteneur-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-sort-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-herbe-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-ingredient-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-livre-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-tache-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-potion-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-rencontresTMR-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-queue-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-souffle-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-tarot-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-tete-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-ombre-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-monnaie-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-meditation-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/competence-carac-defaut.html',
      'systems/foundryvtt-reve-de-dragon/templates/competence-base.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-aspect-tarot.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-categorie-competence.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-categorie-ingredient.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-categorie-parade.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-categorie-vehicule.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-competence.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-initpremierround.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-rarete.html',
      'systems/foundryvtt-reve-de-dragon/templates/sort-draconic.html',
      'systems/foundryvtt-reve-de-dragon/templates/sort-tmr.html',
      'systems/foundryvtt-reve-de-dragon/templates/niveau-ethylisme.html',
      'systems/foundryvtt-reve-de-dragon/templates/casetmr-specific-list.html',
      // Dialogs
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-ajustements.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-resolution.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-competence.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-carac.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-sort.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-encaisser.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-meditation.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-tmr.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-surenc.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-enctotal.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-alchimie.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-astrologie-joueur.html',
      // Calendrier
      'systems/foundryvtt-reve-de-dragon/templates/calendar-template.html',
      'systems/foundryvtt-reve-de-dragon/templates/calendar-editor-template.html',
      'systems/foundryvtt-reve-de-dragon/templates/heures-select-option.html',
      // Conteneur/item in Actor sheet
      'systems/foundryvtt-reve-de-dragon/templates/actor-inventaire-conteneur.html',
      'systems/foundryvtt-reve-de-dragon/templates/editor-notes-mj.html',
      // HUD
      'systems/foundryvtt-reve-de-dragon/templates/hud-actor-init.html',
      'systems/foundryvtt-reve-de-dragon/templates/hud-actor-attaque.html',
      // messages tchat
      'systems/foundryvtt-reve-de-dragon/templates/chat-infojet.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-poesie.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-info-appel-au-moral.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-demande-defense.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-demande-attaque-particuliere.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-demande-attaque-etotal.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-resultat-appelchance.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-resultat-attaque.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-resultat-encaissement.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-resultat-parade.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-resultat-esquive.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-resultat-competence.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-resultat-general.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-resultat-tache.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-resultat-sort.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-resultat-alchimie.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-actor-turn-summary.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-actor-competence-xp.html',
      'systems/foundryvtt-reve-de-dragon/templates/chat-actor-carac-xp.html'
    ];

    Handlebars.registerHelper('upperFirst', str => Misc.upperFirst(str ?? 'Null'));
    Handlebars.registerHelper('upper', str => str?.toUpperCase() ?? 'NULL');
    Handlebars.registerHelper('le', str => Grammar.articleDetermine(str));
    Handlebars.registerHelper('un', str => Grammar.articleIndetermine(str));
    Handlebars.registerHelper('accord', (genre, ...args) => Grammar.accord(genre, args));
    Handlebars.registerHelper('buildConteneur', (objet) => { return RdDUtility.buildConteneur(objet); });

    return loadTemplates(templatePaths);
  }

  /* -------------------------------------------- */
  static getLimitesArchetypes() {
    return duplicate(limitesArchetypes);
  }

  /* -------------------------------------------- */
  static checkNull(items) {
    if (items && items.length) {
      return items;
    }
    return [];
  }

  /* -------------------------------------------- */
  static getNomEthylisme(niveauEthylisme) {
    let index = -niveauEthylisme;
    return index < 0 ? 'Aucun' : nomEthylisme[index];
  }

  /* -------------------------------------------- */
  static initAfficheContenu(actorId) { // persistent handling of conteneur show/hide
    if (!this.afficheContenu)
      this.afficheContenu = {};
  }
  /* -------------------------------------------- */
  static toggleAfficheContenu(conteneurId) {
    this.afficheContenu[conteneurId] = !this.afficheContenu[conteneurId];
  }
  /* -------------------------------------------- */
  static getAfficheContenu(conteneurId) {
    if (conteneurId)
      return this.afficheContenu[conteneurId];
    return undefined;
  }

  /* -------------------------------------------- */
  static filterItemsPerTypeForSheet(data) {
    data.data.materiel = this.checkNull(data.itemsByType['objet']);
    data.data.conteneurs = this.checkNull(data.itemsByType['conteneur']);
    data.data.armes = this.checkNull(data.itemsByType['arme']);
    data.data.armures = this.checkNull(data.itemsByType['armure']);
    data.data.livres = this.checkNull(data.itemsByType['livre']);
    data.data.potions = this.checkNull(data.itemsByType['potion']);
    data.data.ingredients = this.checkNull(data.itemsByType['ingredient']);
    data.data.munitions = this.checkNull(data.itemsByType['munition']);
    data.data.herbes = this.checkNull(data.itemsByType['herbe']);
    data.data.sorts = this.checkNull(data.itemsByType['sort']);
    data.data.queues = this.checkNull(data.itemsByType['queue']);
    data.data.souffles = this.checkNull(data.itemsByType['souffle']);
    data.data.ombres = this.checkNull(data.itemsByType['ombre']);
    data.data.tetes = this.checkNull(data.itemsByType['tete']);
    data.data.taches = this.checkNull(data.itemsByType['tache']);
    data.data.monnaie = this.checkNull(data.itemsByType['monnaie']);
    data.data.meditations = this.checkNull(data.itemsByType['meditation']);
    data.data.chants = this.checkNull(data.itemsByType['chant']);
    data.data.danses = this.checkNull(data.itemsByType['danse']);
    data.data.musiques = this.checkNull(data.itemsByType['musique']);
    data.data.oeuvres = this.checkNull(data.itemsByType['oeuvre']);
    data.data.jeux = this.checkNull(data.itemsByType['jeu']);
    data.data.recettescuisine = this.checkNull(data.itemsByType['recettecuisine']);
    data.data.recettesAlchimiques = this.checkNull(data.itemsByType['recettealchimique']);
    data.data.objets = data.data.conteneurs.concat(data.data.materiel).concat(data.data.armes).concat(data.data.armures).concat(data.data.munitions).concat(data.data.livres).concat(data.data.potions).concat(data.data.herbes).concat(data.data.ingredients);
  }

  /* -------------------------------------------- */
  static async processItemDropEvent(actorSheet, event) {
    let dragData = JSON.parse(event.dataTransfer.getData("text/plain"));
    console.log(dragData, actorSheet.actor._id);
    let dropID = $(event.target).parents(".item").attr("data-item-id"); // Only relevant if container drop
    let objetId = dragData.id || dragData.data._id;
    if (dragData.type == 'Item') {
      if (dropID) { // Dropped over an item !!!
        if (actorSheet.objetVersConteneur[objetId] != dropID && objetId != dropID) {
          if (actorSheet.actor.validateConteneur(objetId, dropID) && actorSheet.actor.testConteneurCapacite(objetId, dropID)) {
            await actorSheet.actor.enleverDeConteneur(objetId, actorSheet.objetVersConteneur[objetId]);
            await actorSheet.actor.ajouterAConteneur(objetId, dropID);
          }
        }
      }
      if (dragData.actorId && dragData.actorId != actorSheet.actor._id) { // Un acteur est à l'origine de l'item -> deplacement
        console.log("Moving objects");
        actorSheet.actor.moveItemsBetweenActors(objetId, dragData.actorId);
        return false;
      }
      actorSheet.actor.computeEncombrementTotalEtMalusArmure();
    } else if (dragData.type == "Actor") {
      actorSheet.actor.addSubacteur(objetId);
    }
    return true;
  }

  /* -------------------------------------------- */
  static buildArbreDeConteneur(actorSheet, data) {
    actorSheet.objetVersConteneur = {}; // Table de hash locale pour recupération rapide du conteneur parent (si existant)    
    // Attribution des objets aux conteneurs
    for (let conteneur of data.data.conteneurs) {
      conteneur.subItems = [];
      if (!conteneur.data.encTotal) conteneur.data.encTotal = 0;
      //conteneur.data.encTotal = ; Deja calculé
      if (conteneur.data.contenu) {
        for (let id of conteneur.data.contenu) {
          let objet = data.data.objets.find(objet => (id == objet._id));
          if (objet) {
            if (!objet.data.encombrement) objet.data.encombrement = 0; // Auto-fix
            objet.estContenu = true; // Permet de filtrer ce qifui est porté dans le template
            actorSheet.objetVersConteneur[id] = conteneur._id;
            conteneur.data.encTotal += Number(objet.data.encombrement) * Number(((objet.data.quantite) ? objet.data.quantite : 1));
            conteneur.subItems.push(objet);
          }
        }
      }
    }
    // Construit la liste des conteneurs de niveau 1 (c'est à dire non contenu eux-même dans un conteneur)
    let newConteneurs = data.data.conteneurs.filter(function (conteneur, index, arr) { return !conteneur.estContenu });
    data.data.conteneurs = newConteneurs;
    //console.log(newConteneurs);
  }

  /* -------------------------------------------- */
  /** Construit la structure récursive des conteneurs, avec imbrication potentielle
   * 
   */
  static buildConteneur(objet, niveau) {
    if (!niveau) niveau = 1;
    objet.niveau = niveau;
    //console.log("OBJ:", objet);
    let str = Handlebars.partials['systems/foundryvtt-reve-de-dragon/templates/actor-inventaire-conteneur.html']({ item: objet });
    if (objet.type == 'conteneur') {
      //console.log("ITEM DISPLAYED", objet );
      if (this.getAfficheContenu(objet._id)) {
        str = str + "<ul class='item-list alterne-list item-display-show list-item-margin" + niveau + "'>";
      } else {
        str = str + "<ul class='item-list alterne-list item-display-hide list-item-margin" + niveau + "'>";
      }
      for (let subItem of objet.subItems) {
        str = str + this.buildConteneur(subItem, niveau + 1);
      }
      str = str + "</ul>";
    }
    return new Handlebars.SafeString(str);
  }

  /* -------------------------------------------- */
  static getCategorieCompetences() {
    return categorieCompetences;
  }
  static getLevelCategory(category) {
    return categorieCompetences[category].level;
  }
  static getLabelCategory(category) {
    return categorieCompetences[category].label;
  }
  static getCaracArray() {
    return carac_array;
  }
  static getDifficultesLibres() {
    return difficultesLibres;
  }
  static getAjustementsConditions() {
    return ajustementsConditions;
  }
  static getAjustementsEncaissement() {
    return ajustementsEncaissement;
  }

  static getDefinitionsBlessures() {
    return definitionsBlessures;
  }

  /* -------------------------------------------- */
  static getCaracNextXp(value) {
    const nextValue = Number(value) + 1;
    // xp est le coût pour atteindre cette valeur, on regarde donc le coût de la valeur+1
    return RdDUtility.getCaracXp(nextValue);
  }

  static getCaracXp(targetValue) {
    return tableCaracDerivee[targetValue]?.xp ?? 200;
  }

  /* -------------------------------------------- */
  static computeCarac(data) {
    data.carac.force.value = Math.min(data.carac.force.value, parseInt(data.carac.taille.value) + 4);

    data.carac.derobee.value = Math.floor(parseInt(((21 - data.carac.taille.value)) + parseInt(data.carac.agilite.value)) / 2);
    let bonusDomKey = Math.floor((parseInt(data.carac.force.value) + parseInt(data.carac.taille.value)) / 2);
    bonusDomKey = Math.min(Math.max(bonusDomKey, 0), 32); // Clamp de securite

    let tailleData = tableCaracDerivee[bonusDomKey];
    data.attributs.plusdom.value = tailleData.plusdom;

    data.attributs.sconst.value = RdDUtility.calculSConst(data.carac.constitution.value);
    data.attributs.sust.value = tableCaracDerivee[Number(data.carac.taille.value)].sust;

    data.attributs.encombrement.value = (parseInt(data.carac.force.value) + parseInt(data.carac.taille.value)) / 2;
    data.carac.melee.value = Math.floor((parseInt(data.carac.force.value) + parseInt(data.carac.agilite.value)) / 2);
    data.carac.tir.value = Math.floor((parseInt(data.carac.vue.value) + parseInt(data.carac.dexterite.value)) / 2);
    data.carac.lancer.value = Math.floor((parseInt(data.carac.tir.value) + parseInt(data.carac.force.value)) / 2);

    data.sante.vie.max = Math.ceil((parseInt(data.carac.taille.value) + parseInt(data.carac.constitution.value)) / 2);

    data.sante.vie.value = Math.min(data.sante.vie.value, data.sante.vie.max)
    data.sante.endurance.max = Math.max(parseInt(data.carac.taille.value) + parseInt(data.carac.constitution.value), parseInt(data.sante.vie.max) + parseInt(data.carac.volonte.value));
    data.sante.endurance.value = Math.min(data.sante.endurance.value, data.sante.endurance.max);
    data.sante.fatigue.max = data.sante.endurance.max * 2;
    data.sante.fatigue.value = Math.min(data.sante.fatigue.value, data.sante.fatigue.max);

    //Compteurs
    data.reve.reve.max = data.carac.reve.value;
    data.compteurs.chance.max = data.carac.chance.value;
  }

  static calculSConst(constitution) {
    return Number(tableCaracDerivee[Number(constitution)].sconst);
  }

  /* -------------------------------------------- */
  static getSegmentsFatigue(maxEnd) {
    maxEnd = Math.max(maxEnd, 1);
    maxEnd = Math.min(maxEnd, fatigueMatrix.length);
    return fatigueMatrix[maxEnd];
  }

  /* -------------------------------------------- */
  static calculMalusFatigue(fatigue, maxEnd) {
    maxEnd = Math.max(maxEnd, 1);
    maxEnd = Math.min(maxEnd, cumulFatigueMatrix.length);
    let segments = cumulFatigueMatrix[maxEnd];
    for (let i = 0; i < 12; i++) {
      if (fatigue <= segments[i]) {
        return fatigueMalus[i]
      }
    }
    return -7;
  }
  /* -------------------------------------------- */
  // Build the nice (?) html table used to manage fatigue.
  // max should be the endurance max value
  static makeHTMLfatigueMatrix(fatigue, maxEndurance) {
    let segments = this.getSegmentsFatigue(maxEndurance);
    return this.makeHTMLfatigueMatrixForSegment(fatigue, segments);
  }

  static makeHTMLfatigueMatrixForSegment(fatigue, segments) {
    fatigue = Math.max(fatigue, 0);
    fatigue = Math.min(fatigue, segments.fatigueMax);

    let table = $("<table/>").addClass('table-fatigue');
    let segmentIdx = 0;
    let fatigueCount = 0;
    for (var line = 0; line < fatigueLineSize.length; line++) {
      let row = $("<tr/>");
      let segmentsPerLine = fatigueLineSize[line];
      row.append("<td class='fatigue-malus'>" + fatigueLineMalus[line] + "</td>");
      while (segmentIdx < segmentsPerLine) {
        let freeSize = segments[segmentIdx];
        for (let col = 0; col < 5; col++) {
          if (col < freeSize) {
            if (fatigueCount < fatigue)
              row.append("<td class='fatigue-used'>X</td>");


            else
              row.append("<td class='fatigue-free'/>");
            fatigueCount++;
          } else {
            row.append("<td class='fatigue-none'/>");
          }
        }
        row.append("<td class='fatigue-separator'/>");
        segmentIdx = segmentIdx + 1;
      }
      table.append(row);
    }
    return table;
  }

  /* -------------------------------------------- */
  static getLocalisation(type = 'personnage') {
    let result = new Roll("1d20").roll().total;
    let txt = ""
    if (type == 'personnage') {
      if (result <= 3) txt = "Jambe, genou, pied, jarret";
      else if (result <= 7) txt = "Hanche, cuisse, fesse";
      else if (result <= 9) txt = "Ventre, reins";
      else if (result <= 12) txt = "Poitrine, dos";
      else if (result <= 14) txt = "Avant-bras, main, coude";
      else if (result <= 18) txt = "Epaule, bras, omoplate";
      else if (result == 19) txt = "Tête";
      else if (result == 20) txt = "Tête (visage)";
    } else {
      if (result <= 7) txt = "Jambes/Pattes";
      else if (result <= 18) txt = "Corps";
      else if (result <= 20) txt = "Tête";
    }

    return { result: result, label: txt };
  }

  /* -------------------------------------------- */
  static selectEncaissement(degats, mortalite) {
    const table = definitionsEncaissement[mortalite] === undefined ? definitionsEncaissement["mortel"] : definitionsEncaissement[mortalite];
    for (let encaissement of table) {
      if ((encaissement.minimum === undefined || encaissement.minimum <= degats)
        && (encaissement.maximum === undefined || degats <= encaissement.maximum)) {
        return duplicate(encaissement);
      }
    }
    return duplicate(table[0]);
  }


  /* -------------------------------------------- */
  static _evaluatePerte(formula, over20) {
    console.log("_evaluatePerte", formula, over20);
    let perte = new Roll(formula, { over20: over20 });
    perte.evaluate();
    return perte.total;
  }

  /* -------------------------------------------- */
  static currentFatigueMalus(value, max) {
    max = Math.max(1, Math.min(max, 60));
    value = Math.min(max * 2, Math.max(0, value));

    let fatigueTab = fatigueMatrix[max];
    let fatigueRem = value;
    for (let idx = 0; idx < fatigueTab.length; idx++) {
      fatigueRem -= fatigueTab[idx];
      if (fatigueRem <= 0) {
        return fatigueMalus[idx];
      }
    }
    return -7; // This is the max !
  }

  /* -------------------------------------------- */
  static async loadCompendiumNames(compendium) {
    const pack = game.packs.get(compendium);
    let competences;
    await pack.getIndex().then(index => competences = index);
    return competences;
  }

  /* -------------------------------------------- */
  static async loadCompendium(compendium, filter = item => true) {
    let compendiumItems = await RdDUtility.loadCompendiumNames(compendium);

    const pack = game.packs.get(compendium);
    let list = [];
    for (let compendiumItem of compendiumItems) {
      await pack.getEntity(compendiumItem._id).then(it => {
        const item = it.data;
        if (filter(item)) {
          list.push(item);
        }
      });
    };
    return list;
  }

  /* -------------------------------------------- */
  static async responseNombreAstral(data) {
    let actor = game.actors.get(data.id);
    actor.ajouteNombreAstral(data);
  }

  /* -------------------------------------------- */
  static onSocketMesssage(sockmsg) {
    console.log(">>>>> MSG RECV", sockmsg);
    switch (sockmsg.msg) {
      case "msg_gm_chat_message":
        return ChatUtility.handleGMChatMessage(sockmsg.data);
      case "msg_sync_time":
        return game.system.rdd.calendrier.syncPlayerTime(sockmsg.data);
      case "msg_request_nombre_astral":
        return game.system.rdd.calendrier.requestNombreAstral(sockmsg.data);
      case "msg_response_nombre_astral":
        return RdDUtility.responseNombreAstral(sockmsg.data);
      case "msg_tmr_move":
        if (game.user.isGM) {
          let actor = game.actors.get(sockmsg.data.actorId);
          actor.refreshTMRView(sockmsg.data.tmrPos);
        }
    }
  }




  /* -------------------------------------------- */
  static async chatListeners(html) {
    RdDCombat.registerChatCallbacks(html);

    // Gestion spécifique message passeurs
    html.on("click", '.tmr-passeur-coord a', event => {
      let coord = event.currentTarget.attributes['data-tmr-coord'].value;
      let actorId = event.currentTarget.attributes['data-actor-id'].value;
      let actor = game.actors.get(actorId);
      actor.tmrApp.forceDemiRevePosition(coord);
    });
    // Gestion spécifique des sorts en réserve multiples (ie têtes)
    html.on("click", '#sort-reserve', event => {
      let coord = event.currentTarget.attributes['data-tmr-coord'].value;
      let sortId = event.currentTarget.attributes['data-sort-id'].value;
      let actorId = event.currentTarget.attributes['data-actor-id'].value;
      let actor = game.actors.get(actorId);
      actor.tmrApp.lancerSortEnReserve(coord, sortId);
    });
    // Gestion du bouton payer
    html.on("click", '#payer-button', event => {
      let sumdenier = event.currentTarget.attributes['data-somme-denier'].value;
      let quantite = event.currentTarget.attributes['data-quantite'].value;
      let jsondata = event.currentTarget.attributes['data-jsondata']
      let objData
      if (jsondata) {
        objData = JSON.parse(jsondata.value)
      }
      if (game.user.character) {
        game.user.character.payerDenier(sumdenier, objData, quantite);
      } else {
        let msgPayer = "Vous devez avoir un acteur relié pour effectuer le paiement";
        ChatMessage.create({ content: msgPayer, whisper: [game.user] });
      }
    });
  }

  /* -------------------------------------------- */
  static createMonnaie(name, valeur_deniers, img = "", enc = 0.01) {
    let piece = {
      name: name, type: 'monnaie', img: img, _id: randomID(16),
      data: {
        quantite: 0,
        valeur_deniers: valeur_deniers,
        encombrement: enc,
        description: ""
      }
    }
    return piece;
  }

  /* -------------------------------------------- */
  static afficherDemandePayer(som1, som2) {
    som1 = (som1) ? som1.toLowerCase() : "0d";
    som2 = (som2) ? som2.toLowerCase() : "0d";
    let regExp = /(\d+)(\w+)/g;
    let p1 = regExp.exec(som1);
    regExp = /(\d+)(\w+)/g;
    let p2 = regExp.exec(som2);
    let sumd = 0;
    let sums = 0;
    if (p1[2] == 'd') sumd += Number(p1[1]);
    if (p1[2] == 's') sums += Number(p1[1]);
    if (p2[2] == 'd') sumd += Number(p2[1]);
    if (p2[2] == 's') sums += Number(p2[1]);

    let sumtotald = sumd + (sums * 100);
    let msgPayer = "La somme de " + sums + " Sols et " + sumd + " Deniers est à payer, cliquer sur le lien ci-dessous si besoin.<br>";
    msgPayer += "<a id='payer-button' class='chat-card-button' data-somme-denier='" + sumtotald + "'>Payer</a>"
    ChatMessage.create({ content: msgPayer });
  }

  /* -------------------------------------------- */
  static chatDataSetup(content, modeOverride, isRoll = false, forceWhisper) {
    let chatData = {
      user: game.user._id,
      rollMode: modeOverride || game.settings.get("core", "rollMode"),
      content: content
    };

    if (["gmroll", "blindroll"].includes(chatData.rollMode)) chatData["whisper"] = ChatMessage.getWhisperRecipients("GM").map(u => u.id);
    if (chatData.rollMode === "blindroll") chatData["blind"] = true;
    else if (chatData.rollMode === "selfroll") chatData["whisper"] = [game.user];

    if (forceWhisper) { // Final force !
      chatData["speaker"] = ChatMessage.getSpeaker();
      chatData["whisper"] = ChatMessage.getWhisperRecipients(forceWhisper);
    }

    return chatData;
  }

  /* -------------------------------------------- */
  static confirmerSuppressionSubacteur(actorSheet, li) {
    let actorId = li.data("actor-id");
    let actor = game.actors.get(actorId);
    let msgTxt = "<p>Etes vous certain de vouloir supprimer le lien vers ce véhicule/monture/suivant : " + actor.data.name + " ?</p>";
    let d = new Dialog({
      title: "Confirmer la suppression du lien",
      content: msgTxt,
      buttons: {
        delete: {
          icon: '<i class="fas fa-check"></i>',
          label: "Supprimer le lien",
          callback: () => {
            console.log("Delete : ", actorId);
            actorSheet.actor.removeSubacteur(actorId);
            li.slideUp(200, () => actorSheet.render(false));
          }
        },
        cancel: {
          icon: '<i class="fas fa-times"></i>',
          label: "Annuler"
        }
      },
      default: "cancel"
    });
    d.render(true);
  }

  /* -------------------------------------------- */
  static async confirmerSuppression(actorSheet, li) {
    let itemId = li.data("item-id");
    let objet = actorSheet.actor.items.find(item => item._id == itemId);
    let msgTxt = "<p>Etes vous certain de vouloir supprimer cet objet ?";
    let buttons = {
      delete: {
        icon: '<i class="fas fa-check"></i>',
        label: "Supprimer l'objet",
        callback: () => {
          console.log("Delete : ", itemId);
          actorSheet.actor.deleteOwnedItem(itemId);
          li.slideUp(200, () => actorSheet.render(false));
        }
      },
      cancel: {
        icon: '<i class="fas fa-times"></i>',
        label: "Annuler"
      }
    }
    if (objet.data.type == 'conteneur' && objet.data.data.contenu.length > 0) {
      msgTxt += "<br>Cet objet est aussi un conteneur avec du contenu : choisissez l'option de suppression";
      buttons['deleteall'] = {
        icon: '<i class="fas fa-check"></i>',
        label: "Supprimer le conteneur et tout son contenu",
        callback: () => {
          console.log("Delete : ", itemId);
          actorSheet.actor.deleteAllConteneur(itemId);
          li.slideUp(200, () => actorSheet.render(false));
        }
      }
    }
    msgTxt += "</p>";
    let d = new Dialog({
      title: "Confirmer la suppression",
      content: msgTxt,
      buttons: buttons,
      default: "cancel"
    });
    d.render(true);
  }

  /* -------------------------------------------- */
  static afficherHeuresChanceMalchance(heureNaissance) {
    if (heureNaissance) {
      let ajustement = game.system.rdd.calendrier.getAjustementAstrologique(heureNaissance);
      ChatMessage.create({
        content: `A l'heure ${game.system.rdd.calendrier.getCurrentHeure()}, le modificateur de Chance/Malchance pour l'heure de naissance ${heureNaissance} est de : ${ajustement}.`,
        whisper: ChatMessage.getWhisperRecipients("MJ")
      });
    }
    else {
      ui.notifications.warn("Pas d'heure de naissance selectionnée")
    }
  }

  /*-------------------------------------------- */
  static checkThanatosXP(compName) {
    if (compName.includes('Thanatos')) {
      let message = "Vous avez mis des points d'Expérience dans la Voie de Thanatos !<br>Vous devez réduire manuellement d'un même montant d'XP une autre compétence Draconique.";
      ChatMessage.create({
        whisper: ChatMessage.getWhisperRecipients(game.user.name),
        content: message
      });
    }
  }

  /*-------------------------------------------- */
  static async onRenderChatMessage(app, html, msg) {
    // TODO 
    //console.log(app, html, msg);
  }

}
