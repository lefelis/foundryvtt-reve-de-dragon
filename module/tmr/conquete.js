import { Grammar } from "../grammar.js";
import { Misc } from "../misc.js";
import { tmrColors, tmrConstants, tmrTokenZIndex, TMRUtility } from "../tmr-utility.js";
import { Draconique } from "./draconique.js";

export class Conquete extends Draconique {

  constructor() {
    super();
  }

  type() { return 'queue' }
  match(item) { return Draconique.isQueueDragon(item) && Grammar.toLowerCaseNoAccent(item.name).includes('conquete'); }
  manualMessage() { return false }
  async onActorCreateOwned(actor, item) { await this._creerConquete(actor, item); }

  code() { return 'conquete' }
  tooltip(linkData) { return `La ${this.tmrLabel(linkData)} doit être conquise` }
  img() { return 'icons/svg/combat.svg' }

  createSprite(pixiTMR) {
    return pixiTMR.sprite(this.code(),
      {
        zIndex: tmrTokenZIndex.conquete,
        color: tmrColors.queues,
        taille: tmrConstants.full,
        decallage: { x: 2, y: 0 }
      });
  }

  async _creerConquete(actor, queue) {
    let existants = actor.data.items.filter(it => this.isCase(it)).map(it => it.data.coord);
    let possibles = TMRUtility.filterTMR(tmr => !TMRUtility.isCaseHumide(tmr) && !existants.includes(tmr.coord));
    let conquete = Misc.rollOneOf(possibles);
    await this.createCaseTmr(actor, 'Conquête: ' + conquete.label, conquete, queue._id);
  }

  async onConquete(actor, tmr, onRemoveToken) {
    let existants = actor.data.items.filter(it => this.isCase(it, tmr.coord));
    for (let casetmr of existants) {
      await actor.deleteOwnedItem(casetmr.data.sourceid);
      onRemoveToken(tmr, casetmr);
    }
  }
}
