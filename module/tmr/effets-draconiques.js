import { Debordement } from "./debordement.js";
import { FermetureCites } from "./fermeture-cites.js";
import { QueteEaux } from "./quete-eaux.js";
import { TerreAttache } from "./terre-attache.js";
import { ReserveExtensible } from "./reserve-extensible.js";
import { DemiReve } from "./demi-reve.js";
import { TrouNoir } from "./trou-noir.js";
import { Rencontre } from "./rencontre.js";
import { SortReserve } from "./sort-reserve.js";
import { CarteTmr } from "./carte-tmr.js";
import { PontImpraticable } from "./pont-impraticable.js";
import { Draconique } from "./draconique.js";
import { PresentCites } from "./present-cites.js";
import { Desorientation } from "./desorientation.js";
import { Conquete } from "./conquete.js";
import { Pelerinage } from "./pelerinage.js";


export class EffetsDraconiques {
  static carteTmr = new CarteTmr();
  static demiReve = new DemiReve();
  static rencontre = new Rencontre();
  static sortReserve = new SortReserve();
  static debordement = new Debordement();
  static presentCites = new PresentCites();
  static fermetureCites = new FermetureCites();
  static queteEaux = new QueteEaux();
  static reserveExtensible = new ReserveExtensible();
  static terreAttache = new TerreAttache();
  static trouNoir = new TrouNoir();
  static pontImpraticable = new PontImpraticable();
  static desorientation = new Desorientation();
  static conquete = new Conquete();
  static pelerinage = new Pelerinage();

  static init() {
    Draconique.register(EffetsDraconiques.carteTmr);
    Draconique.register(EffetsDraconiques.demiReve);
    Draconique.register(EffetsDraconiques.rencontre);
    Draconique.register(EffetsDraconiques.sortReserve);
    Draconique.register(EffetsDraconiques.debordement);
    Draconique.register(EffetsDraconiques.fermetureCites);
    Draconique.register(EffetsDraconiques.queteEaux);
    Draconique.register(EffetsDraconiques.reserveExtensible);
    Draconique.register(EffetsDraconiques.terreAttache);
    Draconique.register(EffetsDraconiques.trouNoir);
    Draconique.register(EffetsDraconiques.pontImpraticable);
    Draconique.register(EffetsDraconiques.presentCites);
    Draconique.register(EffetsDraconiques.desorientation);
    Draconique.register(EffetsDraconiques.conquete);
    Draconique.register(EffetsDraconiques.pelerinage);
  }

  /* -------------------------------------------- */
  static isCaseInondee(caseTMR, coord) {
    return EffetsDraconiques.debordement.isCase(caseTMR, coord) ||
      EffetsDraconiques.pontImpraticable.isCase(caseTMR, coord);
  }

  static isInnaccessible(caseTMR, coord) {
    return EffetsDraconiques.trouNoir.isCase(caseTMR, coord) ||
      EffetsDraconiques.desorientation.isCase(caseTMR, coord);
  }

  static isCaseTrouNoir(caseTMR, coord) {
    return EffetsDraconiques.trouNoir.isCase(caseTMR, coord);
  }

  static isCasePelerinage(caseTMR, coord) {
    return EffetsDraconiques.pelerinage.isCase(caseTMR, coord);
  }

  static isReserveExtensible(caseTMR, coord) {
    return EffetsDraconiques.reserveExtensible.isCase(caseTMR, coord);
  }

  static isTerreAttache(caseTMR, coord) {
    return EffetsDraconiques.terreAttache.isCase(caseTMR, coord);
  }

  static isCiteFermee(caseTMR, coord) {
    return EffetsDraconiques.fermetureCites.isCase(caseTMR, coord);
  }

  static isPresentCite(caseTMR, coord) {
    return EffetsDraconiques.presentCites.isCase(caseTMR, coord);
  }
  /* -------------------------------------------- */
  static isMauvaiseRencontre(element) {
    return EffetsDraconiques.isMatching(element, it => Draconique.isQueueSouffle(it) && it.name.toLowerCase().includes('mauvaise rencontre'));
  }

  static isMonteeLaborieuse(element) {
    return EffetsDraconiques.isMatching(element, it => Draconique.isQueueSouffle(it) && it.name.toLowerCase().includes('montée laborieuse'));
  }

  /* -------------------------------------------- */
  static isFermetureCite(element) {
    return EffetsDraconiques.isMatching(element, it => EffetsDraconiques.fermetureCites.match(it));
  }

  static isPontImpraticable(element) {
    return EffetsDraconiques.isMatching(element, it => EffetsDraconiques.pontImpraticable.match(it));
  }

  static isDoubleResistanceFleuve(element) {
    return EffetsDraconiques.isMatching(element, it => Draconique.isSouffleDragon(it) && it.name.toLowerCase().includes('résistance du fleuve'));
  }

  static isPeage(element) {
    return EffetsDraconiques.isMatching(element, it => Draconique.isSouffleDragon(it) && it.name.toLowerCase().includes('péage'));
  }

  static isPeriple(element) {
    // TODO
    return EffetsDraconiques.isMatching(element, it => Draconique.isSouffleDragon(it) && ir.name.toLowerCase() == 'périple');
  }

  static isDesorientation(element) {
    return EffetsDraconiques.isMatching(element, it => EffetsDraconiques.desorientation.match(it));    // TODO
  }

  /* -------------------------------------------- */
  static isSortImpossible(element) {
    return EffetsDraconiques.isMatching(element, it => EffetsDraconiques.conquete.match(it) || EffetsDraconiques.pelerinage.match(it));
  }

  static isConquete(element) {
    return EffetsDraconiques.isMatching(element, it => EffetsDraconiques.conquete.match(it));
  }

  static isPelerinage(element) {
    return EffetsDraconiques.isMatching(element, it => EffetsDraconiques.pelerinage.match(it));
  }

  static countInertieDraconique(element) {
    return EffetsDraconiques.count(element, it => Draconique.isQueueDragon(it) && it.name.toLowerCase().includes('inertie draconique'));
  }

  static isUrgenceDraconique(element) {
    return EffetsDraconiques.isMatching(element, it => Draconique.isQueueDragon(it) && it.name.toLowerCase() == 'urgence draconique');
  }

  /* -------------------------------------------- */
  static isDonDoubleReve(element) {
    return EffetsDraconiques.isMatching(element, it => Draconique.isTeteDragon(it) && it.name == 'Don de double-rêve');
  }

  static isConnaissanceFleuve(element) {
    return EffetsDraconiques.isMatching(element, it => Draconique.isTeteDragon(it) && it.name.toLowerCase().includes('connaissance du fleuve'));
  }

  static isReserveEnSecurite(element) {
    return EffetsDraconiques.isMatching(element, it => Draconique.isTeteDragon(it) && it.name.toLowerCase().includes(' en sécurité'));
  }

  static isDeplacementAccelere(element) {
    return EffetsDraconiques.isMatching(element, it => Draconique.isTeteDragon(it) && it.name.toLowerCase().includes(' déplacement accéléré'));
  }

  static isMatching(element, matcher) {
    return EffetsDraconiques.toItems(element).find(matcher);
  }
  static count(element, matcher) {
    return EffetsDraconiques.toItems(element).filter(matcher).length;
  }

  static toItems(element) {
    return (element?.entity === 'Actor') ? element.data.items : (element?.entity === 'Item') ? [element] : [];
  }

}