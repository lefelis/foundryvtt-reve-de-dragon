import { Grammar } from "../grammar.js";
import { tmrConstants, tmrTokenZIndex, TMRUtility } from "../tmr-utility.js";
import { Draconique } from "./draconique.js";

export class Pelerinage extends Draconique {

  constructor() {
    super();
  }

  type() { return 'queue' }
  match(item) { return Draconique.isQueueDragon(item) && Grammar.toLowerCaseNoAccent(item.name).includes('pelerinage'); }
  manualMessage() { return false }

  async onActorCreateOwned(actor, queue) { 
    let tmr = TMRUtility.getTMRAleatoire();
    await this.createCaseTmr(actor, 'Pèlerinage: ' + tmr.label, tmr, queue._id);
  }
  
  
  code() { return 'pelerinage' }
  tooltip(linkData) { return `Votre pèlerinage en ${this.tmrLabel(linkData)}` }
  img() { return 'systems/foundryvtt-reve-de-dragon/icons/tmr/pelerin.svg' }
  
  createSprite(pixiTMR) {
    return pixiTMR.sprite(this.code(), {
      zIndex: tmrTokenZIndex.conquete,
      alpha: 1,
      taille: tmrConstants.twoThird,
      decallage: tmrConstants.right
    });
  }
  
  async onFinPelerinage(actor, tmr, onRemoveToken) {
    const pelerinages = actor.data.items.filter(it => this.isCase(it, tmr.coord));
    for (let p of pelerinages){
      await actor.deleteOwnedItem(p.data.sourceid);
      onRemoveToken(tmr, p);
    }
  }

}
