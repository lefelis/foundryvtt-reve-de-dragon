/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

import { RdDUtility } from "./rdd-utility.js";
import { HtmlUtility } from "./html-utility.js";
import { Misc } from "./misc.js";

/* -------------------------------------------- */  
export class RdDActorVehiculeSheet extends ActorSheet {

  /** @override */
	static get defaultOptions() {
    RdDUtility.initAfficheContenu();

	  return mergeObject(super.defaultOptions, {
  	  classes: ["rdd", "sheet", "actor"],
  	  template: "systems/foundryvtt-reve-de-dragon/templates/actor-vehicule-sheet.html",
      width: 640,
      height: 720,
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "carac"}],
      dragDrop: [{dragSelector: ".item-list .item", dropSelector: null}]
    });
  }

  /* -------------------------------------------- */  
  _checkNull(items) {
    if (items && items.length) {
      return items;
    }
    return [];
  }

  /* -------------------------------------------- */
  getData() {
    let data = super.getData();

    data.itemsByType = Misc.classify(data.items);

    RdDUtility.filterItemsPerTypeForSheet(data);
    RdDUtility.buildArbreDeConteneur(this, data);

    this.actor.computeEncombrementTotalEtMalusArmure();    
    data.data.isGM = game.user.isGM;
    data.data.surEncombrementMessage = (this.encTotal > data.capacite_encombrement) ? "Sur-Encombrement!" : "";

    console.log("DATA", data);

    return data;
  }
  
  /* -------------------------------------------- */
  async _onDrop(event) {
    let toSuper = await RdDUtility.processItemDropEvent(this, event);
    if ( toSuper) {
      super._onDrop(event);
    }
  }

  /* -------------------------------------------- */
  /** @override */
	activateListeners(html) {
    super.activateListeners(html);

    HtmlUtility._showControlWhen($(".gm-only"), game.user.isGM);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });
    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      RdDUtility.confirmerSuppression(this, li);
    });

    // Display info about queue
    html.find('.conteneur-name a').click((event) => {
      let myID = event.currentTarget.attributes['data-item-id'].value;
      RdDUtility.toggleAfficheContenu(myID);
      this.render(true);
    });

  }
  
  /* -------------------------------------------- */
  /** @override */
  setPosition(options={}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }


  /* -------------------------------------------- */
  /** @override */
  _updateObject(event, formData) {
    // Update the Actor
    return this.object.update(formData);
  }
}
