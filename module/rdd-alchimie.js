/* -------------------------------------------- */
import { Misc } from "./misc.js";

/* -------------------------------------------- */
export class RdDAlchimie {

  /* -------------------------------------------- */
  static processManipulation( recette, actorId = undefined ) {
    //console.log("CALLED", recette, recette.isOwned, actorId );
    let manip = duplicate(recette.data.manipulation);
    let reg1 = new RegExp(/@(\w*){([\w\-]+)}/ig);
    let matchArray = manip.match( reg1 );
    if ( matchArray ) {
      for( let matchStr of matchArray) {
        let reg2 = new RegExp(/@(\w*){([\w\-]+)}/i);
        let result = matchStr.match(reg2);
        //console.log("RESULT ", result);
        if ( result[1] && result[2]) {
          let commande = Misc.upperFirst( result[1] );
          let replacement = this[`_alchimie${commande}`](recette, result[2], actorId);
          manip = manip.replace( result[0], replacement);
        }
      }
    }
    recette.data.manipulation_update = manip;
  }

  /* -------------------------------------------- */
  static _alchimieCouleur( recette, couleurs, actorId ) {
    let replacement
    if ( actorId ) {
      replacement = `<span class="alchimie-tache"><a data-recette-id="${recette._id}" data-actor-id="${actorId}" data-alchimie-tache="couleur" data-alchimie-data="${couleurs}">couleur ${couleurs}</a></span>`;
    } else {
      replacement = `<span class="alchimie-tache">couleur ${couleurs} </span>`;
    }
    return replacement;
  }

  /* -------------------------------------------- */
  static _alchimieConsistance( recette, consistances, actorId ) {
    let replacement
    if ( actorId ) {
      replacement = `<span class="alchimie-tache"><a data-recette-id="${recette._id}" data-actor-id="${actorId}" data-alchimie-tache="consistance" data-alchimie-data="${consistances}">consistance ${consistances}</a></span>`;
    } else {
      replacement = `<span class="alchimie-tache">consistance ${consistances} </span>`;
    }
    return replacement;

  }

  /* -------------------------------------------- */
  static getDifficulte( aspects ) {
    let aspectsArray = aspects.split('-');
    let diff = 0;
    let nbDifferent = 0;
    let aspectsHash = {}
    for (let colconst of aspectsArray) {
      if ( aspectsHash[colconst] ){  // Deja present, augmente difficulté de 1
        diff -= 1;
      } else {
        nbDifferent++;
        aspectsHash[colconst] = colconst; // Keep track
      }
    }
    diff = diff - ((nbDifferent>1) ? nbDifferent : 0); // Ca doit marcher ....
    return Math.min(0, diff); // Pour être sur
  }
}
