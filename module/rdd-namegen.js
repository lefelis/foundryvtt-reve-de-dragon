import { Grammar } from "./grammar.js";
import { Misc } from "./misc.js";

const words = [ 'pore', 'pre', 'flor', 'lane', 'turlu', 'pin', 'a', 'alph', 'i', 'onse', 'iane', 'ane', 'zach', 'arri', 'ba', 'bo', 'bi', 
                'alta', 'par', 'pir', 'zor', 'zir', 'de', 'pol', 'tran', 'no', 'la','al' , 'pul', 'one', 'ner', 'nur' ];

/* -------------------------------------------- */
export class RdDNameGen {

  static getName( msg, params ) {
    let name = Misc.upperFirst( Misc.rollOneOf(words) + Misc.rollOneOf(words) )
    //console.log(name);
    ChatMessage.create( { content: `Nom : ${name}`, whisper: ChatMessage.getWhisperRecipients("GM") } );
  }

}