import { DeDraconique } from "./de-draconique.js";
import { Grammar } from "./grammar.js";
import { Misc } from "./misc.js";
import { TMRUtility } from "./tmr-utility.js";
import { TMRType } from "./tmr-utility.js";

/* -------------------------------------------- */
const typeRencontres = {

  messager: {
    msgSucces: (data) => `Le ${data.rencontre.name} vous propose d'emmener le message de votre un sort à ${data.rencontre.force} cases ${data.tmr.label}.`,
    msgEchec: (data) => `Le ${data.rencontre.name} est pressé et continue son chemin d'une traite sans vous accorder un regard.`,
    postSucces: (tmrDialog, data) => {
      tmrDialog.setStateRencontre(data.rencontre.type);
      tmrDialog.choisirCasePortee(data.tmr.coord, data.rencontre.force);
    },
    poesieSucces: {
      reference: "La chevelure, Charles Baudelaire",
      extrait: `J'irai là-bas où l'arbre et l'homme, pleins de sève,
                <br>Se pâment longuement sous l'ardeur des climats ;
                <br>Fortes tresses, soyez la houle qui m'enlève !`
    },
    poesieEchec: {
      reference: "Rêve de Dragon, Denis Gerfaud",
      extrait: `En réalité, tous les éléments du rêve des Dragons expriment
                le Draconic : chaque pierre, chaque fleur, chaque goutte d'eau,
                chaque nuage est porteur d'un message dans la langue des Dragons`}
  },

  passeur: {
    msgSucces: (data) => `Le ${data.rencontre.name} vous propose de vous transporter à ${data.rencontre.force} cases des ${data.tmr.label}.`,
    msgEchec: (data) => `Le prix que demande le ${data.rencontre.name} est trop élevé, vous êtes réduit à poursuivre votre chemin par vos propres moyens.`,
    postSucces: (tmrDialog, data) => {
      tmrDialog.setStateRencontre(data.rencontre.type);
      tmrDialog.choisirCasePortee(data.tmr.coord, data.rencontre.force);
    },
    poesieSucces: {
      reference: "Femmes damnées (2), Charles Baudelaire",
      extrait: `Comme je descendais des Fleuves impassibles,
                <br>Je ne me sentis plus guidé par les haleurs :
                <br>Des Peaux-Rouges criards les avaient pris pour cibles,
                <br>Les ayant cloués nus aux poteaux de couleurs.`},
    poesieEchec: {
      reference: "Le bateau ivre, Arthur Rimbaud",
      extrait: `Loin des peuples vivants, errantes, condamnées,
                <br>A travers les déserts courez comme les loups ;
                <br>Faites votre destin, âmes désordonnées,
                <br>Et fuyez l'infini que vous portez en vous !`}
  },

  fleur: {
    msgSucces: (data) => `Vous cueillez la ${data.rencontre.name}, son parfum vous apporte ${data.rencontre.force} points de Rêve.`,
    msgEchec: (data) => `La ${data.rencontre.name} se fâne et disparaît entre vos doigts.`,
    postSucces: (tmrDialog, data) => tmrDialog.actor.reveActuelIncDec(data.rencontre.force),
    poesieSucces: {
      reference: "L'Ennemi, Charles Baudelaire",
      extrait: `Et qui sait si les fleurs nouvelles que je rêve
                <br>Trouveront dans ce sol lavé comme une grève
                <br>Le mystique aliment qui ferait leur vigueur ?`},
    poesieEchec: {
      reference: "Une charogne, Charles Baudelaire",
      extrait: `Et le ciel regardait la carcasse superbe
                <br>Comme une fleur s'épanouir.
                <br>La puanteur était si forte, que sur l'herbe
                <br>Vous crûtes vous évanouir.`},
  },

  mangeur: {
    msgSucces: (data) => `Le ${data.rencontre.name} claque de sa machoire dans le vide avant de fuir.`,
    msgEchec: (data) => `Le ${data.rencontre.name} croque votre Rêve ! Il emporte ${data.rencontre.force} de vos points de rêve actuels`,
    postEchec: (tmrDialog, data) => tmrDialog.actor.reveActuelIncDec(-data.rencontre.force),
    poesieSucces: {
      reference: "Conseil, Victor Hugo",
      extrait: `Rois ! la bure est souvent jalouse du velours.
                <br>Le peuple a froid l'hiver, le peuple a faim toujours.
                <br>Rendez-lui son sort plus facile.
                <br>Le peuple souvent porte un bien rude collier.
                <br>Ouvrez l'école aux fils, aux pères l'atelier,
                <br>À tous vos bras, auguste asile !`},
    poesieEchec: {
      reference: "El Desdichado, Gérard de Nerval",
      extrait: `Suis-je Amour ou Phébus ?... Lusignan ou Biron ?
                <br>Mon front est rouge encor du baiser de la Reine ;
                <br>J'ai rêvé dans la Grotte où nage la sirène...`}
  },

  changeur: {
    msgSucces: (data) => `Le ${data.rencontre.name} vaincu accepte de vous déplacer sur une autre ${TMRType[data.tmr.type].name} de votre choix en échange de sa liberté.`,
    msgEchec: (data) => {
      data.newTMR = TMRUtility.getTMRAleatoire(it => it.type = data.tmr.type);
      return `Le ${data.rencontre.name} vous embobine avec des promesses, et vous transporte en ${data.newTMR.label} sans attendre votre avis.`;
    },
    postSucces: (tmrDialog, data) => {
      tmrDialog.setStateRencontre(data.rencontre.type);
      tmrDialog.choisirCaseType(data.tmr.type);
    },
    postEchec: (tmrDialog, data) => tmrDialog.forceDemiRevePosition(data.newTMR.coord),
    poesieSucces: {
      reference: "Caligula - IIIème chant, Gérard de Nerval",
      extrait: `Allez, que le caprice emporte
                <br>Chaque âme selon son désir,
                <br>Et que, close après vous, la porte
                <br>Ne se rouvre plus qu'au plaisir.`},
    poesieEchec: {
      reference: "Rêve de Dragon, Denis Gerfaud",
      extrait: `Les sages ont encore coutume de dire :
                <br>&laquo; Mais comment les Dragons peuvent-ils
                être influencés par une créature qui, tout
                bien considéré, n'existe pas vraiment pour eux,
                qui n'est que le fantasme de leur activité nocturne ? &raquo;`}
  },

  briseur: {
    msgSucces: (data) => `Le ${data.rencontre.name} tente vainement de vous déconcentrer, avant de fuir sans demander son reste.`,
    msgEchec: (data) => `Le ${data.rencontre.name} vous déconcentre au point de briser votre demi-rêve.`,
    postEchec: (tmrDialog, data) => tmrDialog.close(),
    poesieSucces: {
      reference: "Rêve de Dragon, Denis Gerfaud",
      extrait: `La légende affirme que ce sont les Gnomes qui furent
              les premiers haut-rêvants. En observant les pierres précieuses,
              les gemmes qui sont les larmes de joie des Dragons, ils parvinrent à
              en comprendre la langue. Et l'ayant comprise, ils purent s'en servir
              pour influencer le cours du rêve`},
    poesieEchec: {
      reference: "Quand le rêve se brise, Cypora Sebagh",
      extrait: `Quand le rêve se brise,
              <br>Dans la plainte du jour,
              <br>Ma mémoire devient grise
              <br>Et sombre, tour à tour,
              <br>Dans le puits du silence
              <br>Et de la solitude ;
              <br>Elle reprend son errance
              <br>Parmi la multitude.`}
  },

  reflet: {
    msgSucces: (data) => `Le ${data.rencontre.name} s'estompe dans l'oubli.`,
    msgEchec: (data) => `Vous êtes submergé par un ${data.rencontre.name}, les souvenirs vous retiennent tant qu'il ne sera pas vaincu!`,
    poesieSucces: {
      reference: "Une charogne, Charles Baudelaire",
      extrait: `Les formes s'effaçaient et n'étaient plus qu'un rêve,
              <br>Une ébauche lente à venir
              <br>Sur la toile oubliée, et que l'artiste achève
              <br>Seulement par le souvenir.`},
    poesieEchec: {
      reference: "La chevelure, Charles Baudelaire",
      extrait: `Longtemps ! toujours ! ma main dans ta crinière lourde
              <br>Sèmera le rubis, la perle et le saphir,
              <br>Afin qu'à mon désir tu ne sois jamais sourde !
              <br>N'es-tu pas l'oasis où je rêve, et la gourde
              <br>Où je hume à longs traits le vin du souvenir`}
  },

  passeurfou: {
    msgSucces: (data) => `Le ${data.rencontre.name} tente vainement de découvrir où vous avez caché vos réserves. Vous le chassez, et en déroute il part harceler un autre voyageur du rêve.`,
    msgEchec: (data) => TMRRencontres.msgEchecPasseurFou(data),
    postEchec: (tmrDialog, data) => TMRRencontres.postEchecPasseurFou(tmrDialog, data),
    poesieSucces: {
      reference: "Un Fou et un Sage, Jean de La Fontaine",
      extrait: `Certain Fou poursuivait à coups de pierre un Sage.
              <br>Le Sage se retourne et lui dit : Mon ami,
              <br>C'est fort bien fait à toi ; reçois cet écu-ci :
              <br>Tu fatigues assez pour gagner davantage.`},
    poesieEchec: {
      reference: "Guitare, Victor Hugo",
      extrait: `Je la voyais passer de ma demeure,
              <br>Et c'était tout.
              <br>Mais à présent je m'ennuie à toute heure,
              <br>Plein de dégoût,
              <br>Rêveur oisif, l'âme dans la campagne,
              <br>La dague au clou ... –
              <br>Le vent qui vient à travers la montagne
              <br>M'a rendu fou !`}
  },

  tbblanc: {
    msgSucces: (data) => `Le ${data.rencontre.name} souleve une poussière blanche, vous tenez bon, et il tourbillonne en s'éloignant.`,
    msgEchec: (data) => `Le souffle du ${data.rencontre.name} vous déstabilise et vous emmène dans un nuage de poussière.`,
    postEchec: (tmrDialog, data) => TMRRencontres.onPostEchecTourbillon(tmrDialog, data, 1),
    poesieSucces: {
      reference: "Rêve de Dragon, Denis Gerfaud",
      extrait: `Le Premier Âge fut appelé l'Âge des Dragons. Ce fut le commencement
              des temps, le commencement des rêves. Durant cette période plus mythique
              que réellement historique, les Dragons aimaient à se rêver eux-mêmes.`},
    poesieEchec: {
      reference: "Les Djinns, Victor Hugo",
      extrait: `C'est l'essaim des Djinns qui passe,
              <br>Et tourbillonne en sifflant !
              <br>Les ifs, que leur vol fracasse,
              <br>Craquent comme un pin brûlant.`},
  },

  tbnoir: {
    msgSucces: (data) => `Le ${data.rencontre.name} orageux vous enveloppe de fureur et d'éclairs, vous tenez bon face à la tempête qui s'éloigne sans vous éloigner de votre chemin.`,
    msgEchec: (data) => `Le ${data.rencontre.name} furieux vous secoue tel un fichu de paille malmené par les vents, et vous emporte dans la tourmente.`,
    postEchec: (tmrDialog, data) => TMRRencontres.onPostEchecTourbillon(tmrDialog, data, 2),
    poesieSucces: {
      reference: "Rêve de Dragon, Denis Gerfaud",
      extrait: `Car le Second Âge fut bel et bien celui des Magiciens. Durant cette période, les
              Gnomes s'enfoncèrent profondément sous les montagnes et la magie passa aux
              mains des Humains qui en usèrent et abusèrent, se croyant devenus les maîtres du monde`},
    poesieEchec: {
      reference: "Lily, Pierre Perret",
      extrait: `Elle aurait pas cru sans le voir
              <br>Que la couleur du désespoir
              <br>Là-bas aussi ce fût le noir.`},
  },

  tbrouge: {
    msgSucces: (data) => `Le ${data.rencontre.name} s'abat avec violence mais vous êtes plus rapide et parvenez à lui échapper.`,
    msgEchec: (data) => `Le ${data.rencontre.name} vous frappe de milliers de morsure et vous malmène à travers les terres médianes.`,
    postEchec: (tmrDialog, data) => TMRRencontres.onPostEchecTourbillonRouge(tmrDialog, data),
    poesieSucces: {
      reference: "Qu'est-ce de votre vie ? une bouteille molle, Jean-Baptiste Chassignet",
      extrait: `Qu'est-ce de votre vie ? un tourbillon rouant
              <br>De fumière à flot gris, parmi l'air se jouant,
              <br>Qui passe plus soudain que foudre meurtrière.`},
    poesieEchec: {
      reference: "Les Djinns, poème Victor Hugo",
      extrait: `Cris de l'enfer! voix qui hurle et qui pleure !
              <br>L'horrible essaim, poussé par l'aquilon,
              <br>Sans doute, ô ciel ! s'abat sur ma demeure.
              <br>Le mur fléchit sous le noir bataillon.
              <br>La maison crie et chancelle penchée,
              <br>Et l'on dirait que, du sol arrachée,
              <br>Ainsi qu'il chasse une feuille séchée,
              <br>Le vent la roule avec leur tourbillon !`},
  },

  rdd: {
    msgSucces: (data) => `A tout seigneur, tout honneur, vous faites face à un ${data.rencontre.name}. Vous le maîtrisez et récupérez ses rêves. Vous gagnez ses ${data.rencontre.force} points de rêve`,
    msgEchec: (data) => `A tout seigneur, tout honneur, vous faites face à un ${data.rencontre.name}. La rencontre tourne au cauchemar, dans la lutte épique, vous subissez ${data.rolled.isETotal ? 'deux queues' : 'une queue'} de dragon!`,
    postSucces: (tmrDialog, data) => TMRRencontres.onPostSuccessReveDeDragon(tmrDialog, data),
    postEchec: (tmrDialog, data) => TMRRencontres.onPostEchecReveDeDragon(tmrDialog, data),
    poesieSucces: {
      reference: "Rêve de Dragon, Denis Gerfaud",
      extrait: `Le monde est Rêve de Dragons, mais nous ne savons
              <br>ni leur apparence ni qui sont les dragons.
              <br>En dépit de l'iconographie qui les clame
              <br>immenses créatures ailées crachant des flammes`},
    poesieEchec: {
      reference: "El Desdichado, Gérard de Nerval",
      extrait: `Je suis le Ténébreux, – le Veuf, – l'Inconsolé,
              <br>Le Prince d'Aquitaine à la Tour abolie :
              <br>Ma seule Etoile est morte, – et mon luth constellé
              <br>Porte le Soleil noir de la Mélancolie.`}
  },
}

/* -------------------------------------------- */
const mauvaisesRencontres = [
  { code: "mangeur", name: "Mangeur de Rêve", type: "mangeur", genre: "m", force: "1d6", refoulement: 2, isMauvaise: true },
  { code: "mangeur2d6", name: "Mangeur de Rêve", type: "mangeur", genre: "m", force: "2d6", refoulement: 2, isMauvaise: true },
  { code: "reflet+4", name: "Reflet d'ancien Rêve", type: "reflet", genre: "m", force: "2d6+4", refoulement: 2, isPersistant: true, isMauvaise: true },
  { code: "tbblanc+4", name: "Tourbillon blanc", type: "tbblanc", genre: "m", force: "2d6+4", refoulement: 2, isPersistant: true, isMauvaise: true },
  { code: "tbnoir+4", name: "Tourbillon noir", type: "tbnoir", genre: "m", force: "2d8+4", refoulement: 2, isPersistant: true, isMauvaise: true },
  { code: "passfou", name: "Passeur fou", type: "passeurfou", genre: "m", force: "2d8", refoulement: 2, isMauvaise: true },
  { code: "tbrouge", name: "Tourbillon rouge", type: "tbrouge", genre: "m", force: "2d8", refoulement: 3, isPersistant: true, isMauvaise: true }
]

/* -------------------------------------------- */
const rencontresStandard = [
  { code: "messager", name: "Messager des Rêves", type: "messager", genre: "m", force: "2d4", ignorer: true },
  { code: "passeur", name: "Passeur des Rêves", type: "passeur", genre: "m", force: "2d4", ignorer: true },
  { code: "fleur", name: "Fleur des Rêves", type: "fleur", genre: "f", force: "1d6", ignorer: true },
  { code: "mangeur", name: "Mangeur de Rêve", type: "mangeur", genre: "m", force: "1d6" },
  { code: "changeur", name: "Changeur de Rêve", type: "changeur", genre: "m", force: "2d6" },
  { code: "briseur", name: "Briseur de Rêve", type: "briseur", genre: "m", force: "2d6", quitterTMR: true },
  { code: "reflet", name: "Reflet d'ancien Rêve", type: "reflet", genre: "m", force: "2d6", isPersistant: true },
  { code: "tbblanc", name: "Tourbillon blanc", type: "tbblanc", genre: "m", force: "2d6", isPersistant: true },
  { code: "tbnoir", name: "Tourbillon noir", type: "tbnoir", genre: "m", force: "2d8", isPersistant: true },
  { code: "rdd", name: "Rêve de Dragon", type: "rdd", genre: "m", force: "1ddr + 7", refoulement: 2, quitterTMR: true }
];

const rencontresPresentCite = [
  { code: "messager2d6", name: "Messager des Rêves", type: "messager", genre: "m", force: "2d6", ignorer: true },
  { code: "passeur2d6", name: "Passeur des Rêves", type: "passeur", genre: "m", force: "2d6", ignorer: true },
  { code: "fleur2d6", name: "Fleur des Rêves", type: "fleur", genre: "f", force: "2d6", ignorer: true }
]
const rencontresAll = [].concat(rencontresStandard).concat(mauvaisesRencontres).concat(rencontresPresentCite);

const tableRencontres = {
  cite: [{ code: 'messager', range: [1, 25] }, { code: 'passeur', range: [26, 50] }, { code: 'fleur', range: [51, 65] }, { code: 'mangeur', range: [66, 70] }, { code: 'changeur', range: [71, 80] }, { code: 'briseur', range: [81, 85] }, { code: 'reflet', range: [86, 90] }, { code: 'tbblanc', range: [91, 94] }, { code: 'tbnoir', range: [95, 97] }, { code: 'rdd', range: [98, 100] }],
  sanctuaire: [{ code: 'messager', range: [1, 25] }, { code: 'passeur', range: [26, 50] }, { code: 'fleur', range: [51, 65] }, { code: 'mangeur', range: [66, 70] }, { code: 'changeur', range: [71, 80] }, { code: 'briseur', range: [81, 85] }, { code: 'reflet', range: [86, 90] }, { code: 'tbblanc', range: [91, 94] }, { code: 'tbnoir', range: [95, 97] }, { code: 'rdd', range: [98, 100] }],
  plaines: [{ code: 'messager', range: [1, 20] }, { code: 'passeur', range: [21, 40] }, { code: 'fleur', range: [41, 55] }, { code: 'mangeur', range: [56, 60] }, { code: 'changeur', range: [61, 75] }, { code: 'briseur', range: [76, 82] }, { code: 'reflet', range: [83, 88] }, { code: 'tbblanc', range: [89, 93] }, { code: 'tbnoir', range: [94, 97] }, { code: 'rdd', range: [98, 100] }],
  pont: [{ code: 'messager', range: [1, 20] }, { code: 'passeur', range: [21, 40] }, { code: 'fleur', range: [41, 55] }, { code: 'mangeur', range: [56, 60] }, { code: 'changeur', range: [61, 75] }, { code: 'briseur', range: [76, 82] }, { code: 'reflet', range: [83, 88] }, { code: 'tbblanc', range: [89, 93] }, { code: 'tbnoir', range: [94, 97] }, { code: 'rdd', range: [98, 100] }],
  collines: [{ code: 'messager', range: [1, 15] }, { code: 'passeur', range: [16, 30] }, { code: 'fleur', range: [31, 42] }, { code: 'mangeur', range: [43, 54] }, { code: 'changeur', range: [55, 69] }, { code: 'briseur', range: [70, 82] }, { code: 'reflet', range: [83, 88] }, { code: 'tbblanc', range: [89, 93] }, { code: 'tbnoir', range: [94, 97] }, { code: 'rdd', range: [98, 100] }],
  foret: [{ code: 'messager', range: [1, 15] }, { code: 'passeur', range: [16, 30] }, { code: 'fleur', range: [31, 42] }, { code: 'mangeur', range: [43, 54] }, { code: 'changeur', range: [55, 69] }, { code: 'briseur', range: [70, 82] }, { code: 'reflet', range: [83, 88] }, { code: 'tbblanc', range: [89, 93] }, { code: 'tbnoir', range: [94, 97] }, { code: 'rdd', range: [98, 100] }],
  monts: [{ code: 'messager', range: [1, 10] }, { code: 'passeur', range: [11, 20] }, { code: 'fleur', range: [21, 26] }, { code: 'mangeur', range: [27, 44] }, { code: 'changeur', range: [45, 59] }, { code: 'briseur', range: [60, 75] }, { code: 'reflet', range: [76, 85] }, { code: 'tbblanc', range: [86, 92] }, { code: 'tbnoir', range: [93, 97] }, { code: 'rdd', range: [98, 100] }],
  desert: [{ code: 'messager', range: [1, 10] }, { code: 'passeur', range: [11, 20] }, { code: 'fleur', range: [21, 26] }, { code: 'mangeur', range: [27, 44] }, { code: 'changeur', range: [45, 59] }, { code: 'briseur', range: [60, 75] }, { code: 'reflet', range: [76, 85] }, { code: 'tbblanc', range: [86, 92] }, { code: 'tbnoir', range: [93, 97] }, { code: 'rdd', range: [98, 100] }],
  fleuve: [{ code: 'messager', range: [1, 5] }, { code: 'passeur', range: [6, 10] }, { code: 'fleur', range: [11, 13] }, { code: 'mangeur', range: [14, 37] }, { code: 'changeur', range: [38, 49] }, { code: 'briseur', range: [50, 65] }, { code: 'reflet', range: [66, 79] }, { code: 'tbblanc', range: [80, 89] }, { code: 'tbnoir', range: [90, 97] }, { code: 'rdd', range: [98, 100] }],
  lac: [{ code: 'messager', range: [1, 5] }, { code: 'passeur', range: [6, 10] }, { code: 'fleur', range: [11, 13] }, { code: 'mangeur', range: [14, 37] }, { code: 'changeur', range: [38, 49] }, { code: 'briseur', range: [50, 65] }, { code: 'reflet', range: [66, 79] }, { code: 'tbblanc', range: [80, 89] }, { code: 'tbnoir', range: [90, 97] }, { code: 'rdd', range: [98, 100] }],
  marais: [{ code: 'messager', range: [1, 2] }, { code: 'passeur', range: [3, 4] }, { code: 'fleur', range: [5, 5] }, { code: 'mangeur', range: [6, 29] }, { code: 'changeur', range: [30, 39] }, { code: 'briseur', range: [40, 60] }, { code: 'reflet', range: [61, 75] }, { code: 'tbblanc', range: [76, 86] }, { code: 'tbnoir', range: [87, 97] }, { code: 'rdd', range: [98, 100] }],
  gouffre: [{ code: 'messager', range: [1, 2] }, { code: 'passeur', range: [3, 4] }, { code: 'fleur', range: [5, 5] }, { code: 'mangeur', range: [6, 29] }, { code: 'changeur', range: [30, 39] }, { code: 'briseur', range: [40, 60] }, { code: 'reflet', range: [61, 75] }, { code: 'tbblanc', range: [76, 86] }, { code: 'tbnoir', range: [87, 97] }, { code: 'rdd', range: [98, 100] }],
  necropole: [{ code: 'mangeur', range: [1, 20] }, { code: 'changeur', range: [21, 30] }, { code: 'briseur', range: [31, 50] }, { code: 'reflet', range: [51, 65] }, { code: 'tbblanc', range: [66, 80] }, { code: 'tbnoir', range: [81, 97] }, { code: 'rdd', range: [98, 100] }],
  desolation: [{ code: 'mangeur', range: [1, 20] }, { code: 'changeur', range: [21, 30] }, { code: 'briseur', range: [31, 50] }, { code: 'reflet', range: [51, 65] }, { code: 'tbblanc', range: [66, 80] }, { code: 'tbnoir', range: [81, 97] }, { code: 'rdd', range: [98, 100] }]
}


/* -------------------------------------------- */
export class TMRRencontres {
  static gestionRencontre = {}

  /* -------------------------------------------- */
  static init() {
    for (let type in typeRencontres) {
      TMRRencontres.register(type, typeRencontres[type]);
    }
  }

  /* -------------------------------------------- */
  static register(type, rencontre) {
    TMRRencontres.gestionRencontre[type] = rencontre;
  }

  /* -------------------------------------------- */
  /**
   * Retourne une recontre en fonction de la case et du tirage
   * @param {*} terrain 
   * @param {*} roll 
   */
  static async rollRencontre(terrain, roll = undefined) {
    if (!terrain) {
      ChatMessage.create({ content: "Un type de case doit être indiqué (par exemple sanctuaire, desert ou cité)" });
      return false;
    }
    if (!roll || roll <= 0 || roll > 100) {
      roll = new Roll("1d100").evaluate().total;
    }
    let rencontre = await TMRRencontres.getRencontreAleatoire(terrain, roll);
    ChatMessage.create({
      user: game.user._id,
      whisper: [game.user._id],
      content: `Rencontre en ${terrain} (jet : ${roll}%)<br>Vous rencontrez un ${rencontre.name} de ${rencontre.force} Points de Rêve`
    });
    return false;
  }

  /* -------------------------------------------- */
  static getRencontre(index) {
    let rencontre;
    if (isNaN(index)) {
      rencontre = rencontresAll.find(r => r.type == index) ?? rencontresAll.find(r => r.code == index)
    }
    else if (0 <= index && index < rencontresAll.length) {
      rencontre = rencontresAll[index];
    }
    if (rencontre) {
      return duplicate(rencontre);
    }
    else {
    ui.notifications.info(`Pas de rencontre pour ${index}, seulement ${rencontresAll.length} rencontres sont connues.<br>Vous pouvez aussi essayer par type (ie: mangeur, fleur, fleur2d6, ...)`)
    }
    return undefined;
  }

  /* -------------------------------------------- */
  static async getRencontreAleatoire(terrain, roll = undefined) {
    if (!roll || roll <= 0 || roll > 100) {
      roll = new Roll("1d100").evaluate().total;
    }
    terrain = Grammar.toLowerCaseNoAccent(terrain);
    //console.log("getRencontreAleatoire", terrain, roll);
    const code = tableRencontres[terrain].find(it => it.range[0] <= roll && roll <= it.range[1]).code;
    const rencontre = duplicate(rencontresStandard.find(it => it.code == code));
    rencontre.roll = roll;
    await TMRRencontres.evaluerForceRencontre(rencontre);
    //console.log(rencontre);
    return rencontre;
  }

  /* -------------------------------------------- */
  static async getMauvaiseRencontre(index = undefined) {
    const rencontre = duplicate(
      (index && index >= 0 && index < mauvaisesRencontres.length)
        ? mauvaisesRencontres[index]
        : Misc.rollOneOf(mauvaisesRencontres));
    await TMRRencontres.evaluerForceRencontre(rencontre);
    return rencontre;
  }

  /* -------------------------------------------- */
  static async evaluerForceRencontre(rencontre) {
    if (TMRRencontres.isReveDeDragon(rencontre)) {
      const ddr = await DeDraconique.ddr("selfroll")
      rencontre.force = 7 + ddr.total;
    }
    else {
      rencontre.force = new Roll(rencontre.force).evaluate().total;
    }
    return rencontre.force;
  }

  /* -------------------------------------------- */
  static isReveDeDragon(rencontre) {
    return rencontre.type == "rdd";
  }

  /* -------------------------------------------- */
  static getGestionRencontre(name) {
    let gestion = TMRRencontres.gestionRencontre[name];
    if (!gestion) {
      ui.notifications.error(`La rencontre ${name} est inconnue, pas de méthode de gestion associée`)
      gestion = TMRRencontres.gestionRencontre['messager'];
    }
    return gestion;
  }

  /* -------------------------------------------- */
  static async gererRencontre(tmrDialog, data) {
    let gestion = TMRRencontres.getGestionRencontre(data.rencontre.type);
    if (data.rolled.isSuccess) {
      data.message = gestion.msgSucces(data);
      if (data.nbRounds > 1) {
        data.message += ` Au total, vous avez passé ${data.nbRounds} rounds à vous battre!`;
      }
      data.poesie = gestion.poesieSucces;
      return gestion.postSucces;
    }

    data.message = gestion.msgEchec(data);
    if (data.nbRounds > 1) {
      data.message += ` Vous avez passé ${data.nbRounds} rounds à lutter!`;
    }
    data.poesie = gestion.poesieEchec;
    return gestion.postEchec;
  }

  /* -------------------------------------------- */
  static msgEchecPasseurFou(data) {
    data.sortReserve = data.actor.data.data.reve.reserve.list[0];
    if (data.sortReserve) {
      // Passeur fou positionne sur la case d'un ort en réserve // TODO : Choisir le sort le plus loin ou au hasard
      data.newTMR = TMRUtility.getTMR(data.sortReserve.coord);
    } else {
      // Déplacement aléatoire de la force du Passeur Fou
      const newCoord = Misc.rollOneOf(TMRUtility.getTMRPortee(data.tmr.coord, data.rencontre.force));
      data.newTMR = TMRUtility.getTMR(newCoord);
    }
    if (data.sortReserve) {
      return `Le ${data.rencontre.name} vous dérobe la clé de vos sorts. Vous vous saisissez de lui, mais dans un nuage violet, il vous emporte en ${data.newTMR.label} déclencher votre sort en réserve de ${data.sortReserve.name}.`;
    }
    else {
      return `Le ${data.rencontre.name} tente de vous dérober la clé de vos sorts. Ne la trouvant pas, il déclenche un nuage violet et vous emporte en ${data.newTMR.label}`;
    }
  }

  static async postEchecPasseurFou(tmrDialog, data) {
    if (data.sortReserve) {
      await tmrDialog.processSortReserve(data.sortReserve);
    }
    await tmrDialog.forceDemiRevePosition(data.newTMR.coord);
    if (data.sortReserve) {
      tmrDialog.close();
    }
  }

  /* -------------------------------------------- */
  static async onPostEchecTourbillon(tmrDialog, data, cases) {
    await data.actor.reveActuelIncDec(-cases);
    await TMRRencontres._toubillonner(tmrDialog, data.actor, cases);
  }

  /* -------------------------------------------- */
  static async onPostEchecTourbillonRouge(tmrDialog, data) {
    await data.actor.reveActuelIncDec(-2); // -2 pts de Reve a chaque itération
    TMRRencontres._toubillonner(tmrDialog, data.actor, 4);
    await data.actor.santeIncDec("vie", -1); // Et -1 PV
  }

  static async _toubillonner(tmrDialog, actor, cases) {
    let coord = actor.data.data.reve.tmrpos.coord;
    for (let i = 0; i < cases; i++) {
      coord = TMRUtility.deplaceTMRAleatoire(actor, coord).coord;
    }
    await tmrDialog.forceDemiRevePosition(coord)
  }

  static async onPostSuccessReveDeDragon(tmrDialog, data) {
    if (data.rolled.isPart) {
      await data.actor.appliquerExperience(data.rolled, 'reve', data.competence);
    }
    await data.actor.resultCombatReveDeDragon(data);
  }

  static async onPostEchecReveDeDragon(tmrDialog, data) {
    await data.actor.resultCombatReveDeDragon(data);
    tmrDialog.close();
  }
}

