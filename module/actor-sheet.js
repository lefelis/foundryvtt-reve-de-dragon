/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

import { RdDUtility } from "./rdd-utility.js";
import { HtmlUtility } from "./html-utility.js";
import { RdDItemArme } from "./item-arme.js";
import { RdDItemCompetence } from "./item-competence.js";
import { RdDBonus } from "./rdd-bonus.js";
import { Misc } from "./misc.js";
import { RdDCombatManager } from "./rdd-combat.js";

/* -------------------------------------------- */
export class RdDActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    RdDUtility.initAfficheContenu();
    return mergeObject(super.defaultOptions, {
      classes: ["rdd", "sheet", "actor"],
      template: "systems/foundryvtt-reve-de-dragon/templates/actor-sheet.html",
      width: 640,
      //height: 720,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "carac" }],
      dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }],
      editCaracComp: false,
      showCompNiveauBase: false,
      montrerArchetype: false
    });
  }

  /* -------------------------------------------- */
  getData() {
    let data = super.getData();
    if ( data.actor.type == 'creature' || data.actor.type == 'humanoide') return data; // Shortcut

    data.data.editCaracComp = this.options.editCaracComp;
    data.data.showCompNiveauBase = this.options.showCompNiveauBase;
    data.data.montrerArchetype = this.options.montrerArchetype;

    data.itemsByType = Misc.classify(data.items);

    // Competence per category
    data.data.comptageArchetype = RdDUtility.getLimitesArchetypes();
    data.data.competenceXPTotal = 0;
    data.competenceByCategory = Misc.classify(
      data.itemsByType.competence,
      item => item.data.categorie,
      item => {
        let archetypeKey = (item.data.niveau_archetype < 0) ? 0 : item.data.niveau_archetype;
        if (data.data.comptageArchetype[archetypeKey] == undefined) {
          data.data.comptageArchetype[archetypeKey] = { "niveau": archetypeKey, "nombreMax": 0, "nombre": 0};
        }
        data.data.comptageArchetype[archetypeKey].nombre = (data.data.comptageArchetype[archetypeKey]?.nombre??0) + 1; //Comptage archetype
        item.data.xpNext = RdDItemCompetence.getCompetenceNextXp(item.data.niveau);
        item.data.isLevelUp = item.data.xp >= item.data.xpNext; // Flag de niveau à MAJ
        //this.actor.checkCompetenceXP(item.name); // Petite vérification experience
        item.data.showCompetence = !data.data.showCompNiveauBase || (Number(item.data.niveau) != Number(RdDUtility.getLevelCategory(item.data.categorie)));
        // Ignorer les compétences 'troncs' à ce stade
        data.data.competenceXPTotal += RdDItemCompetence.computeCompetenceXPCost(item);
        return item;
      });
    data.data.competenceXPTotal -= RdDItemCompetence.computeEconomieCompetenceTroncXP(data.itemsByType.competence);

    // Compute current carac sum
    let sum = 0;
    for (let caracName in data.data.carac) {
      let currentCarac = data.data.carac[caracName];
      if (!currentCarac.derivee) {
        sum += parseInt(currentCarac.value);      
      }
      currentCarac.xpNext = RdDUtility.getCaracNextXp(currentCarac.value);
      currentCarac.isLevelUp = (currentCarac.xp >= currentCarac.xpNext);
    }
    sum += (data.data.beaute >= 0) ? (data.data.beaute - 10) : 0;
    data.data.caracSum = sum;

    // Force empty arme, at least for Esquive
    if (data.itemsByType.arme == undefined) data.itemsByType.arme = [];
    for (const arme of data.itemsByType.arme) {
      arme.data.niveau = 0; // Per default, TODO to be fixed
      for (const melee of data.competenceByCategory.melee) {
        if (melee.name == arme.data.competence)
          arme.data.niveau = melee.data.niveau
      }
      for (const tir of data.competenceByCategory.tir) {
        if (tir.name == arme.data.competence)
          arme.data.niveau = tir.data.niveau
      }
      for (const lancer of data.competenceByCategory.lancer) {
        if (lancer.name == arme.data.competence)
          arme.data.niveau = lancer.data.niveau
      }
    }

    // To avoid armour and so on...
    data.data.combat = duplicate(RdDUtility.checkNull(data.itemsByType['arme']));
    data.data.combat = RdDCombatManager.finalizeArmeList(data.data.combat, data.itemsByType.competence, data.data.carac);

    data.esquive = { name: "Esquive", niveau: data.competenceByCategory?.melee.find(it => it.name == 'Esquive')?.data.niveau ?? -6};
    let corpsACorps = data.competenceByCategory?.melee.find(it => it.name == 'Corps à corps');
    if (corpsACorps) {
      let cc_init = RdDCombatManager.calculInitiative(corpsACorps.data.niveau, data.data.carac['melee'].value);
      data.data.combat.push(RdDItemArme.mainsNues({ niveau: corpsACorps.data.niveau, initiative: cc_init }));
    }
    this.armesList = duplicate(data.data.combat);

    data.data.carac.taille.isTaille = true; // To avoid button link;
    data.data.compteurs.chance.isChance = true;
    data.data.blessures.resume = this.actor.computeResumeBlessure(data.data.blessures);

    // Mise à jour de l'encombrement total et du prix de l'équipement
    this.actor.computeEncombrementTotalEtMalusArmure();
    this.actor.computePrixTotalEquipement();
    
    // Common data
    data.data.competenceByCategory = data.competenceByCategory;
    data.data.encTotal = this.actor.encTotal;
    data.data.prixTotalEquipement = this.actor.prixTotalEquipement;
    data.data.surprise = RdDBonus.find(this.actor.getSurprise(false)).descr;
    data.data.isGM = game.user.isGM;
    data.ajustementsConditions = CONFIG.RDD.ajustementsConditions;
    data.difficultesLibres = CONFIG.RDD.difficultesLibres;

    // low is normal, this the base used to compute the grid.
    data.data.fatigue = {
      malus: RdDUtility.calculMalusFatigue(data.data.sante.fatigue.value, data.data.sante.endurance.max),
      html: "<table class='table-fatigue'>" + RdDUtility.makeHTMLfatigueMatrix(data.data.sante.fatigue.value, data.data.sante.endurance.max).html() + "</table>"
    }

    RdDUtility.filterItemsPerTypeForSheet(data);
    data.data.sortReserve = data.data.reve.reserve.list;
    data.data.rencontres  = duplicate(data.data.reve.rencontre.list);
    data.data.caseSpeciales = data.itemsByType['casetmr'];
    RdDUtility.buildArbreDeConteneur(this, data);
    data.data.surEncombrementMessage = (data.data.compteurs.surenc.value < 0) ? "Sur-Encombrement!" : "";
    data.data.vehiculesList = this.actor.buildVehiculesList();
    data.data.monturesList  = this.actor.buildMonturesList();
    data.data.suivantsList  = this.actor.buildSuivantsList();
    return data;
  }

  /* -------------------------------------------- */
  async _onDrop(event) {
    let toSuper = await RdDUtility.processItemDropEvent(this, event);
    if ( toSuper) {
      super._onDrop(event);
    }
  }

  /* -------------------------------------------- */
  async createEmptyTache() {
    await this.actor.createOwnedItem({ name: 'Nouvelle tache', type: 'tache' }, { renderSheet: true });
  }

  /* -------------------------------------------- */
  async creerObjet() {
    let itemType = $("#creer-equipement").val();
    await this.actor.createOwnedItem({ name: 'Nouveau ' + itemType, type: itemType }, { renderSheet: true });
  }

  /* -------------------------------------------- */
  async selectObjetType() {
    let itemType = ["objet", "arme", "armure", "conteneur", "herbe", "ingredient", "livre", "potion", "munition", "monnaie"];
    let options = '<span class="competence-label">Selectionnez le type d\'équipement</span><select id="creer-equipement">';
    for (let typeName of itemType) {
      options += '<option value="' + typeName + '">' + typeName + '</option>'
    }
    options += '</select>';
    let d = new Dialog({
      title: "Créer un équipement",
      content: options,
      buttons: {
        one: {
          icon: '<i class="fas fa-check"></i>',
          label: "Créer l'objet",
          callback: () => this.creerObjet()
        }
      }
    });
    d.render(true);
  }

  /* -------------------------------------------- */
  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    HtmlUtility._showControlWhen($(".gm-only"), game.user.isGM);

    html.find('#show-hide-competences').click((event) => {
      this.options.showCompNiveauBase = !this.options.showCompNiveauBase;
      this.render(true);
    });

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("item-id"));
      item.sheet.render(true);
    });
    // Update Inventory Item
    html.find('.rencontre-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const rencontreKey = li.data("item-id");
      this.actor.deleteTMRRencontre(rencontreKey);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      RdDUtility.confirmerSuppression(this, li);
    });    
    html.find('.subacteur-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      RdDUtility.confirmerSuppressionSubacteur(this, li);
    });
    
    html.find('#encaisser-direct').click(ev => {
      this.actor.encaisser();
    });

    html.find('.remise-a-neuf').click(ev => {
      if (game.user.isGM) {
        this.actor.remiseANeuf();
        ev.preventDefault();
      }
    });
    html.find('#creer-tache').click(ev => {
      this.createEmptyTache();
    });
    html.find('#creer-un-objet').click(ev => {
      this.selectObjetType();
    });
    html.find('#nettoyer-conteneurs').click(ev => {
      this.actor.nettoyerConteneurs();
    });

    // Blessure control
    html.find('.blessure-control').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      let btype = li.data("blessure-type");
      let index = li.data('blessure-index');
      let active = $(ev.currentTarget).data('blessure-active');
      //console.log(btype, index, active);
      this.actor.manageBlessureFromSheet(btype, index, active).then(this.render(true));
    });

    // Blessure data
    html.find('.blessures-soins').change(ev => {
      const li = $(ev.currentTarget).parents(".item");
      let btype = li.data('blessure-type');
      let index = li.data('blessure-index');
      let psoins = li.find('input[name=premiers_soins]').val();
      let pcomplets = li.find('input[name=soins_complets]').val();
      let jours = li.find('input[name=jours]').val();
      let loc = li.find('input[name=localisation]').val();
      //console.log(btype, index, psoins, pcomplets, jours, loc);
      this.actor.setDataBlessureFromSheet(btype, index, psoins, pcomplets, jours, loc).then(this.render(true));
    });

    // Equip Inventory Item
    html.find('.item-equip').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.equiperObjet(li.data("item-id"));
      this.render(true);
    });

    // Roll Carac
    html.find('.carac-label a').click((event) => {
      let caracName = event.currentTarget.attributes.name.value;
      this.actor.rollCarac(caracName.toLowerCase());
    });

    html.find('#chance-actuelle').click((event) => {
      this.actor.rollCarac('chance-actuelle');
    });

    html.find('#chance-appel').click((event) => {
      this.actor.rollAppelChance();
    });

    html.find('#jet-astrologie').click((event) => {
      this.actor.astrologieNombresAstraux();
    });

    // Roll Skill
    html.find('.competence-label a').click((event) => {
      let compName = event.currentTarget.text;
      this.actor.rollCompetence(compName);
    });
    html.find('.tache-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let tacheId = li.data('item-id');
      this.actor.rollTache(tacheId);
    });
    html.find('.meditation-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let meditationId = li.data('item-id');
      this.actor.rollMeditation(meditationId);
    });
    html.find('.chant-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let chantId = li.data('item-id');
      this.actor.rollChant(chantId);
    });
    html.find('.danse-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let danseId = li.data('item-id');
      this.actor.rollDanse(danseId);
    });
    html.find('.musique-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let musiqueId = li.data('item-id');
      this.actor.rollMusique(musiqueId);
    });
    html.find('.oeuvre-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let oeuvreId = li.data('item-id');
      this.actor.rollOeuvre(oeuvreId);
    });
    html.find('.jeu-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let jeuId = li.data('item-id');
      this.actor.rollJeu(jeuId);
    });
    html.find('.recettecuisine-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let recetteId = li.data('item-id');
      this.actor.rollRecetteCuisine(recetteId);
    });
    html.find('.subacteur-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let actorId = li.data('actor-id');
      let actor = game.actors.get( actorId) ;
      if ( actor ) {
        actor.sheet.render(true);
      }
    });
    
    // Points de reve actuel
    html.find('.ptreve-actuel a').click((event) => {
      this.actor.rollCarac('reve-actuel');
    });

    // Roll Weapon1
    html.find('.arme-label a').click((event) => {
      let armeName = event.currentTarget.text;
      let competenceName = event.currentTarget.attributes['data-competence-name'].value;
      this.actor.rollArme(competenceName, armeName);
    });
    // Initiative pour l'arme
    html.find('.arme-initiative a').click((event) => {
      let combatant = game.combat.data.combatants.find(c => c.actor.data._id == this.actor.data._id);
      if (combatant) {
        let armeName = event.currentTarget.attributes['data-arme-name'].value;
        let arme = this.armesList.find(a => a.name == armeName);
        RdDCombatManager.rollInitiativeCompetence(combatant._id, arme);
      } else {
        ui.notifications.info("Impossible de lancer l'initiative sans être dans un combat.");
      }
    });
    // Display TMR, visuualisation
    html.find('#visu-tmr').click((event) => {
      this.actor.displayTMR("visu");
    });

    // Display TMR, normal
    html.find('#monte-tmr').click((event) => {
      this.actor.displayTMR("normal");
    });

    // Display TMR, fast 
    html.find('#monte-tmr-rapide').click((event) => {
      this.actor.displayTMR("rapide");
    });

    html.find('#dormir-une-heure').click((event) => {
      this.actor.dormir(1);
    });
    html.find('#dormir-chateau-dormant').click((event) => {
      this.actor.dormirChateauDormant();
    });
    html.find('#enlever-tous-effets').click((event) => {
      this.actor.enleverTousLesEffets();
    });
    // Display info about queue
    html.find('.queuesouffle-label a').click((event) => {
      let myID = event.currentTarget.attributes['data-item-id'].value;
      const item = this.actor.getOwnedItem(myID);
      item.sheet.render(true);
    });
    // Info sort
    html.find('.sort-label a').click((event) => {
      let myID = event.currentTarget.attributes['data-id'].value;
      const item = this.actor.getOwnedItem(myID);
      item.sheet.render(true);
    });
    // Info sort
    html.find('.case-label a').click((event) => {
      let myID = event.currentTarget.attributes['data-id'].value;
      const item = this.actor.getOwnedItem(myID);
      item.sheet.render(true);
    });

    // Display info about queue
    html.find('.conteneur-name a').click((event) => {
      let myID = event.currentTarget.attributes['data-item-id'].value;
      RdDUtility.toggleAfficheContenu(myID);
      this.render(true);
    });

    if (this.options.editCaracComp) {
      // On carac change
      html.find('.carac-value').change((event) => {
        let caracName = event.currentTarget.name.replace(".value", "").replace("data.carac.", "");
        //console.log("Value changed :", event, caracName);
        this.actor.updateCarac(caracName, parseInt(event.target.value));
      });
      html.find('.carac-xp').change((event) => {
        let caracName = event.currentTarget.name.replace(".xp", "").replace("data.carac.", "");
        //console.log("Value changed :", event, caracName);
        this.actor.updateCaracXP(caracName, parseInt(event.target.value));
      });
      // On competence change
      html.find('.competence-value').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        //console.log("Competence changed :", compName);
        this.actor.updateCompetence(compName, parseInt(event.target.value));
      });
      // On competence xp change
      html.find('.competence-xp').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCompetenceXP(compName, parseInt(event.target.value));
      });
      // On competence xp change
      html.find('.competence-xp-sort').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCompetenceXPSort(compName, parseInt(event.target.value));
      });
      // On competence archetype change
      html.find('.competence-archetype').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCompetenceArchetype(compName, parseInt(event.target.value));
      });
    }

    // Gestion du bouton lock/unlock
    html.find('.lock-unlock-sheet').click((event) => {
      this.options.editCaracComp = !this.options.editCaracComp;
      this.render(true);
    });
    html.find('#show-hide-archetype').click((event) => {
      this.options.montrerArchetype = !this.options.montrerArchetype;
      this.render(true);
    });

    // On pts de reve change
    html.find('.pointsreve-value').change((event) => {
      let reveValue = event.currentTarget.value;
      let reve = duplicate(this.actor.data.data.reve.reve);
      reve.value = reveValue;
      this.actor.update({ "data.reve.reve": reve });
    });

    // On seuil de reve change
    html.find('.seuil-reve-value').change((event) => {
      console.log("seuil-reve-value", event.currentTarget)
      this.actor.setPointsDeSeuil(event.currentTarget.value);
    });

    html.find('#attribut-protection-edit').change((event) => {
      this.actor.updateProtectionValue(event.currentTarget.attributes.name.value, parseInt(event.target.value));
    });

    // On stress change
    html.find('.compteur-edit').change((event) => {
      let fieldName = event.currentTarget.attributes.name.value;
      this.actor.updateCompteurValue(fieldName, parseInt(event.target.value));
    });

    html.find('#ethylisme').change((event) => {
      this.actor.setEthylisme(parseInt(event.target.value));
    });
    html.find('#stress-test').click((event) => {
      this.actor.transformerStress();
      this.render(true);
    });
    html.find('#moral-malheureux').click((event) => {
      this.actor.jetDeMoral('malheureuse');
      this.render(true);
    });
    html.find('#moral-neutre').click((event) => {
      this.actor.jetDeMoral('neutre');
      this.render(true);
    });
    html.find('#moral-heureux').click((event) => {
      this.actor.jetDeMoral('heureuse');
      this.render(true);
    });
    html.find('#ethylisme-test').click((event) => {
      this.actor.ethylismeTest();
      this.render(true);
    });

    html.find('#jet-vie').click((event) => {
      this.actor.jetVie();
      this.render(true);
    });
    html.find('#jet-endurance').click((event) => {
      this.actor.jetEndurance();
      this.render(true);
    });

    html.find('.monnaie-plus').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      this.actor.monnaieIncDec(li.data("item-id"), 1);
      this.render(true);
    });
    html.find('.monnaie-moins').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      this.actor.monnaieIncDec(li.data("item-id"), -1);
      this.render(true);
    });

    html.find('#vie-plus').click((event) => {
      this.actor.santeIncDec("vie", 1);
      this.render(true);
    });
    html.find('#vie-moins').click((event) => {
      this.actor.santeIncDec("vie", -1);
      this.render(true);
    });
    html.find('#endurance-plus').click((event) => {
      this.actor.santeIncDec("endurance", 1);
      this.render(true);
    });
    html.find('#endurance-moins').click((event) => {
      this.actor.santeIncDec("endurance", -1);
      this.render(true);
    });
    html.find('.data-sante-sonne').click((event) => {
      this.actor.setSonne(event.currentTarget.checked);
      this.render(true);
    });
    html.find('#ptreve-actuel-plus').click((event) => {
      this.actor.reveActuelIncDec(1);
      this.render(true);
    });
    html.find('#ptreve-actuel-moins').click((event) => {
      this.actor.reveActuelIncDec(-1);
      this.render(true);
    });
    html.find('#fatigue-plus').click((event) => {
      this.actor.santeIncDec("fatigue", 1);
      this.render(true);
    });
    html.find('#fatigue-moins').click((event) => {
      this.actor.santeIncDec("fatigue", -1);
      this.render(true);
    });
  }


  /* -------------------------------------------- */
  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }


  /* -------------------------------------------- */
  /** @override */
  _updateObject(event, formData) {
    // Update the Actor
    return this.object.update(formData);
  }
}
