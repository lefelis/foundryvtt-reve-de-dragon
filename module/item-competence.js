
const competenceTroncs = [["Esquive", "Dague", "Corps à corps"],
["Epée à 1 main", "Epée à 2 mains", "Hache à 1 main", "Hache à 2 mains", "Lance", "Masse à 1 main", "Masse à 2 mains"]];

const competence_xp_par_niveau = [5, 5, 5, 10, 10, 10, 10, 15, 15, 15, 15, 20, 20, 20, 20, 30, 30, 40, 40, 60, 60, 100, 100, 100, 100, 100, 100, 100, 100, 100];
const competence_niveau_max = competence_xp_par_niveau.length - 10;

function _buildCumulXP() {
  let cumulXP = { "-11": 0 };
  let cumul = 0;
  for (let i = 0; i <= competence_xp_par_niveau.length; i++) {
    let level = i - 10;
    cumul += competence_xp_par_niveau[i];
    cumulXP[level] = cumul;
  }
  return cumulXP;
}

const competence_xp_cumul = _buildCumulXP();

export class RdDItemCompetence extends Item {

  /* -------------------------------------------- */
  static isCompetenceArme(competence) {
    switch (competence.data.categorie) {
      case 'melee':
        return competence.name.toLowerCase() != 'esquive';
      case 'tir':
      case 'lancer':
        return true;
    }
    return false;
  }

  /* -------------------------------------------- */
  static isArmeUneMain(competence) {
    return competence?.name.toLowerCase().includes("1 main");
  }
  static isArme2Main(competence) {
    return competence?.name.toLowerCase().includes("2 main");
  }

  /* -------------------------------------------- */
  static isMalusEncombrementTotal(competence) {
    return competence?.name.toLowerCase().match(/(natation|acrobatie)/);
  }

  /* -------------------------------------------- */
  static isTronc(compName) {
    for (let troncList of competenceTroncs) {
      for (let troncName of troncList) {
        if (troncName == compName)
          return troncList;
      }
    }
    return false;
  }
  /* -------------------------------------------- */
  static computeCompetenceXPCost(competence) {
    let xp = RdDItemCompetence.getDeltaXp(competence.data.base, competence.data.niveau ?? competence.data.base);
    xp += competence.data.xp ?? 0;
    if ( competence.name.includes('Thanatos') ) xp *= 2; /// Thanatos compte double !
    xp += competence.data.xp_sort ?? 0;
    return xp;
  }

  /* -------------------------------------------- */
  static computeEconomieCompetenceTroncXP(competences) {
    let economie = 0;
    for (let troncList of competenceTroncs) {
      let list = troncList.map(name => RdDItemCompetence.findCompetence(competences, name))
        .sort( (c1, c2) => c2.data.niveau - c1.data.niveau); // tri du plus haut au plus bas
      list.splice(0,1); // ignorer la plus élevée
      list.forEach(c => {
        economie += RdDItemCompetence.getDeltaXp(c.data.base, Math.min(c.data.niveau, 0) );
      });
    }
    return economie;
  }

  /* -------------------------------------------- */
  static findCompetence(list, name) {
    name = name.toLowerCase();
    return list.find(item => item.name.toLowerCase() == name && (item.type == "competence" || item.type == "competencecreature"))
  }

  /* -------------------------------------------- */
  static getCompetenceNextXp(niveau) {
    return RdDItemCompetence.getCompetenceXp(niveau + 1);
  }

  /* -------------------------------------------- */
  static getCompetenceXp(niveau) {
    RdDItemCompetence._valideNiveau(niveau);
    return niveau < -10 ? 0 : competence_xp_par_niveau[niveau + 10];
  }

  /* -------------------------------------------- */
  static getDeltaXp(from, to) {
    RdDItemCompetence._valideNiveau(from);
    RdDItemCompetence._valideNiveau(to);
    return competence_xp_cumul[to] - competence_xp_cumul[from];
  }

  /* -------------------------------------------- */
  static _valideNiveau(niveau){
    if (niveau < -11 || niveau > competence_niveau_max) {
      console.warn("Niveau en dehors des niveaux de compétences: [-11, " + competence_niveau_max + "]", niveau)
    }
  }

}