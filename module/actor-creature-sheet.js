
/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

import { HtmlUtility } from "./html-utility.js";
import { RdDUtility } from "./rdd-utility.js";
import { RdDActorSheet } from "./actor-sheet.js";

/* -------------------------------------------- */
export class RdDActorCreatureSheet extends RdDActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["rdd", "sheet", "actor"],
      template: "systems/foundryvtt-reve-de-dragon/templates/actor-creature-sheet.html",
      width: 640,
      height: 720,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "carac" }],
      dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }]
    });
  }


  /* -------------------------------------------- */
  getData() {
    let data = super.getData();
    console.log("Creature : ", data);

    data.itemsByType = {};
    for (const item of data.items) {
      let list = data.itemsByType[item.type];
      if (!list) {
        list = [];
        data.itemsByType[item.type] = list;
      }
      list.push(item);
    }

    // Compute current carac sum
    let sum = 0;
    Object.values(data.data.carac).forEach(carac => { if (!carac.derivee) { sum += parseInt(carac.value) } });
    data.data.caracSum = sum;

    data.data.carac.taille.isTaille = true; // To avoid button link;
    data.data.blessures.resume = this.actor.computeResumeBlessure(data.data.blessures);

    data.data.isGM = game.user.isGM;

    data.data.competencecreature = data.itemsByType["competencecreature"];

    this.actor.computeEncombrementTotalEtMalusArmure();
    RdDUtility.filterItemsPerTypeForSheet(data);
    RdDUtility.buildArbreDeConteneur(this, data);
    data.data.encTotal = this.actor.encTotal;
    data.data.isGM = game.user.isGM;

    console.log("Creature : ", this.objetVersConteneur, data);

    return data;
  }

  /* -------------------------------------------- */
  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    HtmlUtility._showControlWhen($(".gm-only"), game.user.isGM);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // On competence change
    html.find('.creature-carac').change((event) => {
      let compName = event.currentTarget.attributes.compname.value;
      this.actor.updateCreatureCompetence(compName, "carac_value", parseInt(event.target.value));
    });
    html.find('.creature-niveau').change((event) => {
      let compName = event.currentTarget.attributes.compname.value;
      this.actor.updateCreatureCompetence(compName, "niveau", parseInt(event.target.value));
    });
    html.find('.creature-dommages').change((event) => {
      let compName = event.currentTarget.attributes.compname.value;
      this.actor.updateCreatureCompetence(compName, "dommages", parseInt(event.target.value));
    });
  }

  /* -------------------------------------------- */

  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }

  /* -------------------------------------------- */
  /** @override */
  _updateObject(event, formData) {
    // Update the Actor
    return this.object.update(formData);
  }
}
