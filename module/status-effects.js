
const demiReveStatusEffect = { id: 'demi-reve', rdd: true, label: 'Demi-rêve', icon: 'systems/foundryvtt-reve-de-dragon/icons/heures/hd12.svg' };
const rddStatusEffects = [
  { id: 'sonne', rdd: true, label: 'Sonné', icon: 'icons/svg/stoned.svg' },
  demiReveStatusEffect
];
const statusDemiSurprise = new Set(['sonne', 'prone', 'restrain']);
const statusSurpriseTotale = new Set(['unconscious', 'blind', 'dead']);

export class StatusEffects {
  static onReady() {
    StatusEffects.setCoreStatusId([demiReveStatusEffect]);
    StatusEffects.setCoreStatusId(rddStatusEffects);
    StatusEffects.setMandatoryRdd();
    const defaultUseStatusEffect = CONFIG.statusEffects.map(it => it.id).join();
    game.settings.register("foundryvtt-reve-de-dragon", "use-status-effects", {
      name: "use-status-effects",
      scope: "world",
      config: false,
      default: defaultUseStatusEffect,
      type: String
    });

    game.settings.registerMenu("foundryvtt-reve-de-dragon", "select-status-effect", {
      name: "Choisir les effets disponibles",
      label: "Choix des effets",
      hint: "Ouvre la fenêtre de sélection des effets/status appliqués aux acteurs",
      icon: "fas fa-bars",
      type: StatusEffectsSettings,
      restricted: true
    });
    CONFIG.RDD.allEffects = rddStatusEffects.concat(CONFIG.statusEffects);
    
    StatusEffects._setUseStatusEffects(StatusEffects._getUseStatusEffects());
    console.log('statusEffects', CONFIG.statusEffects);
  }

  static valeurSurprise(effect, isCombat) {
    const id = StatusEffects.statusId(effect);
    if (statusSurpriseTotale.has(id)) {
      return 2;
    }
    return statusDemiSurprise.has(id) || (isCombat && id == demiReveStatusEffect.id) ? 1 : 0;
  }

  static statusId(effectData) {
    return effectData.flags?.core?.statusId ?? effectData["flags.core.statusId"];
  }

  static setCoreStatusId(list) {
    list.forEach(it => {
      it.flags = { core: { statusId: it.id } };
      it["flags.core.statusId"] = it.id;
    });
  }
  static setMandatoryRdd() {
    CONFIG.statusEffects.filter(it => statusDemiSurprise.has(it.id) || statusSurpriseTotale.has(it.id))
      .forEach(it => it.rdd = true);
  }

  static _getUseStatusEffects() {
    const setting = game.settings.get("foundryvtt-reve-de-dragon", "use-status-effects");
    return setting ? new Set(setting.split(',')) : new Set();
  }

  static _setUseStatusEffects(useStatusEffects) {
    if (game.user.isGM) {
      game.settings.set("foundryvtt-reve-de-dragon", "use-status-effects", StatusEffects._toSetting(useStatusEffects));
    }

    for (let effect of CONFIG.RDD.allEffects) {
      effect.active = effect.rdd || useStatusEffects.has(effect.id);
    }
    CONFIG.statusEffects = CONFIG.RDD.allEffects.filter(it => it.active);
  }

  static _toSetting(useStatusEffects) {
    return Array.from(useStatusEffects).join();
  }

  static demiReve() {
    return demiReveStatusEffect;
  }
}

class StatusEffectsSettings extends FormApplication {
  constructor(...args) {
    super(...args);
  }

  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      id: "status-effects-settings",
      template: "systems/foundryvtt-reve-de-dragon/templates/status-effects-settings.html",
      height: "800",
      width: 350,
      minimizable: false,
      closeOnSubmit: true,
      title: "Choix des status/effets"
    });
    return options;
  }

  getData() {
    let data = super.getData();
    data.effects = CONFIG.RDD.allEffects;
    return data;
  }

  activateListeners(html) {
    html.find(".select-effect").click((event) => {
      let id = event.currentTarget.attributes.name?.value;
      if (id) {
        let selected = StatusEffects._getUseStatusEffects();
        let isChecked = event.currentTarget.checked;
        if (isChecked) {
          selected.add(id);
        }
        else {
          selected.delete(id);
        }
        StatusEffects._setUseStatusEffects(selected);
      }
    });
  }

  async _updateObject(event, formData) {
    this.close();
  }
}

